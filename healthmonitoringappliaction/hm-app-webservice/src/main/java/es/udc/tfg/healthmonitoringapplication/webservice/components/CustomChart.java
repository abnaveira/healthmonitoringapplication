package es.udc.tfg.healthmonitoringapplication.webservice.components;

import be.eliwan.tapestry5.high.components.Highcharts;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementTimeRange;
import es.udc.tfg.healthmonitoringapplication.model.services.measurementservice.MeasurementService;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UserSession;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CustomChart extends Highcharts {

    @Property
    @Parameter(required=true, defaultPrefix= BindingConstants.LITERAL)
    private String title;

    @Property
    @Parameter(required=true, defaultPrefix= BindingConstants.LITERAL)
    private String dateFormat;

    @Property
    @Parameter(required=true, defaultPrefix= BindingConstants.LITERAL)
    private String heartRate;

    @Property
    @Parameter(required=true, defaultPrefix= BindingConstants.LITERAL)
    private String steps;

    @Property
    @Parameter(required=true, defaultPrefix= BindingConstants.LITERAL)
    private String calories;

    @Property
    @Parameter(required=true, defaultPrefix= BindingConstants.LITERAL)
    private String distance;

    @Property
    @Parameter(required=true, defaultPrefix= BindingConstants.LITERAL)
    private String filter;

    @Property
    @Parameter(required=true, defaultPrefix= BindingConstants.LITERAL)
    private Long usrId;

    @Inject
    private MeasurementService measurementService;



    @Override
    public JSONObject getComponentOptions() {

        List<List<Measurement>> measurementsInAllRangesById = measurementService.getMeasurementsInAllRangesById(usrId);

        JSONObject base = super.getComponentOptions();

        base.put("title", new JSONObject("text", title));
        ((JSONObject) base.get("chart")).put("type", "column");

        JSONObject xAxis = new JSONObject();
        xAxis.put("type","datetime");
        xAxis.put("labels",new JSONObject("format",dateFormat));
        base.put("xAxis", xAxis);

        List<Measurement> Daymeasurements = measurementsInAllRangesById.get(MeasurementTimeRange.valueOf(filter).ordinal());
        base.put("series", new JSONArray(getHeartRate(Daymeasurements),getSteps(Daymeasurements),getCalories(Daymeasurements),getDistance(Daymeasurements)));

        return base;
    }

     JSONObject getHeartRate(List<Measurement> measurements){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name",heartRate);
//         jsonObject.put("color","red");

        JSONArray jsonArray = new JSONArray();
        int k = measurements.size();
        for(int i =0;i<k;i++){
            jsonArray.put(new JSONArray(measurements.get(i).getDate().getTimeInMillis(),measurements.get(i).getHeartRate()));
        }
        jsonObject.put("data",jsonArray);
        return jsonObject;
    }

     JSONObject getSteps(List<Measurement> measurements){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name",steps);
//         jsonObject.put("color","blue");

         JSONArray jsonArray = new JSONArray();
        int k = measurements.size();
        for(int i =0;i<k;i++){
            jsonArray.put(new JSONArray(measurements.get(i).getDate().getTimeInMillis(),measurements.get(i).getSteps()));
        }
        jsonObject.put("data",jsonArray);
        return jsonObject;
    }

    JSONObject getCalories(List<Measurement> measurements){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name",calories);
//        jsonObject.put("color","green");


        JSONArray jsonArray = new JSONArray();
        int k = measurements.size();
        for(int i =0;i<k;i++){
            jsonArray.put(new JSONArray(measurements.get(i).getDate().getTimeInMillis(),measurements.get(i).getCalories()));
        }
        jsonObject.put("data",jsonArray);
        return jsonObject;
    }

    JSONObject getDistance(List<Measurement> measurements){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name",distance);
//        jsonObject.put("color","orange");

        JSONArray jsonArray = new JSONArray();
        int k = measurements.size();
        for(int i =0;i<k;i++){
            jsonArray.put(new JSONArray(measurements.get(i).getDate().getTimeInMillis(),measurements.get(i).getDistance()));
        }
        jsonObject.put("data",jsonArray);
        return jsonObject;
    }

}