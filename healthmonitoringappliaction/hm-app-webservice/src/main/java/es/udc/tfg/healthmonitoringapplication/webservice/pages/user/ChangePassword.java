package es.udc.tfg.healthmonitoringapplication.webservice.pages.user;

import es.udc.tfg.healthmonitoringapplication.model.exceptions.AuthenticationException;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.webservice.pages.Index;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicy;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicyType;
import es.udc.tfg.healthmonitoringapplication.webservice.util.CookiesManager;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UserSession;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Cookies;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class ChangePassword {

    @Property
    private String oldPassword;

    @Property
    private String newPassword;

    @Property
    private String retypeNewPassword;

    @SessionState(create=false)
    private UserSession userSession;

    @Component
    private Form changePasswordForm;

    @Inject
    private Cookies cookies;

    @Inject
    private Messages messages;

    @Inject
    private UserProfileService userProfileService;

    /*se hacen las comprobaciones necesarias y se llama al servicio para cambiar la contraseña*/
    void onValidateFromChangePasswordForm() throws InstanceNotFoundException {

        if (!changePasswordForm.isValid()) {
            return;
        }

        if (!newPassword.equals(retypeNewPassword)) {
            changePasswordForm
                    .recordError(messages.get("error-passwordsDontMatch"));
        } else {

            try {
                userProfileService.changePassword(userSession.getUserProfileId(), oldPassword, newPassword);
            } catch (AuthenticationException e) {
                changePasswordForm.recordError(messages
                        .get("error-invalidPassword"));
            }

        }

    }

    Object onSuccess() {

        CookiesManager.removeCookies(cookies);
        return Index.class;

    }
}