package es.udc.tfg.healthmonitoringapplication.webservice.services;


import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthenticationPolicy {
    AuthenticationPolicyType value() default AuthenticationPolicyType.ALL_USERS;
}
