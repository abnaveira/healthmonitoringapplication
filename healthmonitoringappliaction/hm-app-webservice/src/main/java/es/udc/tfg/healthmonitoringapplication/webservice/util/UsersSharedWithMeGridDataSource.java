package es.udc.tfg.healthmonitoringapplication.webservice.util;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import java.util.List;

/*Esta clase (y el resto de clases similares) se usan para crear las tablas que muestran información
 * para ello se implementa GridDataSource.*/
public class UsersSharedWithMeGridDataSource implements GridDataSource {
    private UserProfileService userProfileService;
    private long usrId;
    private List<UserProfile> userProfiles;
    private int startIndex;

    public UsersSharedWithMeGridDataSource(UserProfileService userProfileService, long usrId) {
        this.userProfileService = userProfileService;
        this.usrId = usrId;
    }

    /*Devolvemos el número de filas totales*/
    @Override
    public int getAvailableRows() {
        return userProfileService.getNumberOfUsersSharedWithMe(usrId);
    }

    /*Con i e i1 podemos establecer la pagina que se accede*/
    @Override
    public void prepare(int i, int i1, List<SortConstraint> list) {
        try {
            userProfiles=userProfileService.getUserProfilesSharedWithMe(usrId,i,i1-i+1);
        } catch (InstanceNotFoundException e) {
            e.printStackTrace();
        }
        this.startIndex=i;

    }

    @Override
    public Object getRowValue(int i) {
        return userProfiles.get(i-this.startIndex);
    }

    @Override
    public Class getRowType() {
        return UserProfile.class;
    }
}
