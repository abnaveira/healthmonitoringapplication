package es.udc.tfg.healthmonitoringapplication.webservice.pages.user;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.webservice.pages.Index;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UserSession;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Register {

    @Property
    private String username;

    @Property
    private String password;

    @Property
    private String retypePassword;

    @Property
    private String name;

    @Property
    private Integer age;

    @Property
    private Integer height;

    @Property
    private Float weight;

    @Property
    private String gender;

    @Property
    private String lifeStyle;

    private long userProfileId;

    @SessionState(create=false)
    private UserSession userSession;

    @Inject
    private UserProfileService userProfileService;

    @Inject
    private RoleService roleService;

    @Component
    private Form registrationForm;

    @Component(id = "username")
    private TextField usernameField;

    @Component(id = "password")
    private PasswordField passwordField;

    @Inject
    private Messages messages;

    /*Devuelve un string que represnta la lista de opciones en el select box para el estilo de vida*/
    public String getLifeStyles(){
        List<LifeStyle> lifeStyles = Arrays.asList(LifeStyle.values());
        StringBuilder s = new StringBuilder();
        for(LifeStyle l: lifeStyles){
            s.append(l.name()).append("=").append(messages.get(l.name())).append(",");
        }
        return s.substring(0,s.length()-1);
    }

    /*Devuelve un string que representa la lista de opciones en el select box para el género*/
    public String getGenders(){
        List<Gender> genders = Arrays.asList(Gender.values());
        StringBuilder s = new StringBuilder();
        for(Gender g: genders){
            s.append(g.name()).append("=").append(messages.get(g.name())).append(",");
        }
        return s.substring(0,s.length()-1);
    }

    /*Se hacen las comprobaciones necesarias, se crea el usuario y se almacena en BD.*/
    void onValidateFromRegistrationForm() {

        if (!registrationForm.isValid()) {
            return;
        }

        if (!password.equals(retypePassword)) {
            registrationForm.recordError(passwordField, messages
                    .get("error-passwordsDontMatch"));
        } else {
            try {
                Role role = roleService.findByRoleName("USER");
                Set<Role> roles = new HashSet<>();
                roles.add(role);
                HealthStats healthStats = new HealthStats(age,height,weight,Gender.valueOf(gender),LifeStyle.valueOf(lifeStyle));
                UserProfile userProfile = new UserProfile(name,username,password,roles);
                userProfile.setHealthStats(healthStats);
                healthStats.setUserProfile(userProfile);
                UserProfile userProfile1 = userProfileService.createUserProfile(userProfile);
                userProfileId=userProfile1.getUsrId();
            } catch (DuplicateInstanceException e) {
                registrationForm.recordError(usernameField, messages.get("error-loginNameAlreadyExists"));
            } catch (InstanceNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    Object onSuccess() {
        /*Cuando se completa el registro exitosamente se crea el userSession y se vuelve a la página de inicio.*/
        userSession = new UserSession();
        userSession.setUserProfileId(userProfileId);
        userSession.setName(name);
        return Index.class;

    }


}