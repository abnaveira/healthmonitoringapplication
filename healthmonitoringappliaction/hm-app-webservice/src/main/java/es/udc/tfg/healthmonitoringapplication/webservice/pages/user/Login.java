package es.udc.tfg.healthmonitoringapplication.webservice.pages.user;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.webservice.pages.Index;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicy;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicyType;
import es.udc.tfg.healthmonitoringapplication.webservice.util.CookiesManager;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UserSession;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Cookies;


@AuthenticationPolicy(AuthenticationPolicyType.NON_AUTHENTICATED_USERS)
public class Login {
    @Property
    private String loginName;

    @Property
    private String password;

    @Property
    private boolean rememberMyPassword;

    @SessionState(create=false)
    private UserSession userSession;

    @Inject
    private Cookies cookies;

    @Component
    private Form loginForm;

    @Inject
    private Messages messages;

    @Inject
    UserProfileService userProfileService;

    private UserProfile userProfile= null;

    void onValidateFromLoginForm() {

        if (!loginForm.isValid()) {
            return;
        }

        try {
            userProfile = userProfileService.login(loginName, password,false);
        } catch (Exception e) {
            loginForm.recordError(messages.get("error-authenticationFailed"));
        }

    }

    Object onSuccess() {
        /*Se crea una sesión donde se guarda el nombre y el id del usuario*/
        userSession = new UserSession();
        userSession.setUserProfileId(userProfile.getUsrId());
        userSession.setName(userProfile.getName());

        /*Si se le da a recordarme, se crea una cookie*/
        if (rememberMyPassword) {
            CookiesManager.leaveCookies(cookies, loginName, userProfile
                    .getPassword());
        }
        /*Se redirige a la página de inicio.*/
        return Index.class;
    }
}