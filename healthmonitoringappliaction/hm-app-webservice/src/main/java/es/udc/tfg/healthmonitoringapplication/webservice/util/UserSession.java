package es.udc.tfg.healthmonitoringapplication.webservice.util;

/*Esta clase sirve para mantener una instancia que dura mientras el usuario esté autenticado y se mantiene al cambiar de página.*/
public class UserSession {
    private Long userProfileId;
    private String name;

    public Long getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(Long userProfileId) {
        this.userProfileId = userProfileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
