package es.udc.tfg.healthmonitoringapplication.webservice.pages.user;


import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.webservice.pages.Index;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicy;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicyType;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UserSession;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import java.util.Arrays;
import java.util.List;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class UserSettings {
    @Property
    private String name;

    @Property
    private Integer age;

    @Property
    private Integer height;

    @Property
    private  Float weight;

    @Property
    private String gender;

    @Property
    private String lifeStyle;

    @SessionState(create=false)
    private UserSession userSession;

    @Inject
    UserProfileService userProfileService;

    @Inject
    private Messages messages;

    /*Devuelve un string que represnta la lista de opciones en el select box para el estilo de vida.*/
    public String getLifeStyles(){
        List<LifeStyle> lifeStyles = Arrays.asList(LifeStyle.values());
        StringBuilder s = new StringBuilder();
        for(LifeStyle l: lifeStyles){
            s.append(l.name()).append("=").append(messages.get(l.name())).append(",");
        }
        return s.substring(0,s.length()-1);
    }

    /*Devuelve un string que representa la lista de opciones en el select box para el género.*/
    public String getGenders(){
        List<Gender> genders = Arrays.asList(Gender.values());
        StringBuilder s = new StringBuilder();
        for(Gender g: genders){
            s.append(g.name()).append("=").append(messages.get(g.name())).append(",");
        }
        return s.substring(0,s.length()-1);
    }

    /*Se rellenan los campos con la información del usuario*/
    void onPrepareForRender() throws InstanceNotFoundException {

        UserProfile userProfile;

        userProfile = userProfileService.findById(userSession
                .getUserProfileId());
        name = userProfile.getName();
        gender=userProfile.getHealthStats().getGender().name();
        lifeStyle=userProfile.getHealthStats().getLifeStyle().name();
        age=userProfile.getHealthStats().getAge();
        height=userProfile.getHealthStats().getHeight();
        weight=userProfile.getHealthStats().getWeight();
    }

    /*Cuando se guarden los cambios se actualiza el perfil en BD.*/
    Object onSuccess() throws InstanceNotFoundException {

        UserProfile userProfile;
        userProfile = userProfileService.findById(userSession.getUserProfileId());
        userProfile.setName(name);
        HealthStats healthStats = userProfile.getHealthStats();
        healthStats.setAge(age);
        healthStats.setHeight(height);
        healthStats.setWeight(weight);
        healthStats.setLifeStyle(LifeStyle.valueOf(lifeStyle));
        healthStats.setGender(Gender.valueOf(gender));
        userProfile.setHealthStats(healthStats);
        userProfileService.updateUserProfile(userProfile);
        userSession.setName(name);
        return Index.class;

    }

}