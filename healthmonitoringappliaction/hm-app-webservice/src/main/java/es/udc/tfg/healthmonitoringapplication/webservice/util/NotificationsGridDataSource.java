package es.udc.tfg.healthmonitoringapplication.webservice.util;

import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.model.services.notificationservice.NotificationService;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/*Esta clase (y el resto de clases similares) se usan para crear las tablas que muestran información
 * para ello se implementa GridDataSource.*/
public class NotificationsGridDataSource implements GridDataSource {
    NotificationService notificationService;
    private long usrId;
    private List<UserNotification> userNotifications;
    private int startIndex;
    private Calendar date;

    public NotificationsGridDataSource(NotificationService notificationService, long usrId, Date date) {
        this.notificationService = notificationService;
        this.usrId = usrId;
        this.date = Calendar.getInstance();
        this.date.setTime(date);
    }
    /*Devolvemos el número de filas totales*/
    @Override
    public int getAvailableRows() {
        return notificationService.getNumberOfNotifications(usrId,date);
    }

    /*Con i e i1 podemos establecer la pagina que se accede*/
    @Override
    public void prepare(int i, int i1, List<SortConstraint> list) {
        userNotifications=notificationService.findUserNotifications(usrId,date,i,i1-i+1);
        this.startIndex=i;

    }

    @Override
    public Object getRowValue(int i) {
        return userNotifications.get(i-this.startIndex);
    }

    @Override
    public Class<UserNotification> getRowType() {
        return UserNotification.class;
    }
}
