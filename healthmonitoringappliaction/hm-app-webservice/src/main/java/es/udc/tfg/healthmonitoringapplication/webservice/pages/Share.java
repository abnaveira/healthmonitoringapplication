package es.udc.tfg.healthmonitoringapplication.webservice.pages;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicy;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicyType;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UserSession;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UsersByKeywordGridDataSource;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UsersISharedWithGridDataSource;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;

import javax.inject.Inject;
import java.util.List;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class Share {
    @SessionState(create=false)
    private UserSession userSession;

    @Property
    private String name;

    @Property
    private UsersByKeywordGridDataSource usersByKeywordGridDataSource;

    @Property
    private UserProfile user;

    @Property
    private UserProfile user2;

    @Inject
    private UserProfileService userProfileService;

    @Property
    private UsersISharedWithGridDataSource usersISharedWithGridDataSource;

    void onActivate(String name) throws InstanceNotFoundException {
        this.name=name;
        /*Se crea el grid datasource para buscar por keywords*/
        usersByKeywordGridDataSource= new UsersByKeywordGridDataSource(userProfileService,name);

    }

    String onPassivate(){
        return name;
    }
    Object onSuccessFromSearchUserForm() {
        return Share.class;
    }

    /*Cuando se clickea en share with*/
    void onActionFromShareWith(Long usrId) throws InstanceNotFoundException {
        userProfileService.shareProfileWith(userSession.getUserProfileId(),usrId);
    }

    /*Cuando se clickea en delete*/
    void onActionFromDelete(Long usrId) throws InstanceNotFoundException {
        userProfileService.cancelShareProfileWith(userSession.getUserProfileId(),usrId);
    }

    void setupRender(){
        /*Se crea el grid de los usuarios con los que se está compartiendo la información.*/
        usersISharedWithGridDataSource= new UsersISharedWithGridDataSource(userProfileService,userSession.getUserProfileId());
    }
}