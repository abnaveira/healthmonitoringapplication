package es.udc.tfg.healthmonitoringapplication.webservice.pages;


import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementTimeRange;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementsStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.measurementservice.MeasurementService;
import es.udc.tfg.healthmonitoringapplication.model.services.notificationservice.NotificationService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicy;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicyType;
import es.udc.tfg.healthmonitoringapplication.webservice.util.*;
import org.apache.tapestry5.annotations.*;
import org.apache.tapestry5.corelib.components.DateField;
import org.apache.tapestry5.corelib.components.Select;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;
import org.apache.tapestry5.services.ajax.JavaScriptCallback;
import org.apache.tapestry5.services.javascript.InitializationPriority;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.tapestry5.corelib.components.Select.CHANGE_EVENT;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class Management {

    private final static int ROWS_PER_PAGE = 10;

    @Inject
    private UserProfileService userProfileService;

    @SessionState(create=false)
    private UserSession userSession;

    @Inject
    private Messages messages;

    @Inject
    private MeasurementService measurementService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Locale locale;

    /*@Property
    private MeasurementGridDataSource measurementGridDataSource;*/

   /* @Property
    private MeasurementsByDateDataSource measurementsByDateDataSource;

    @Property
    private NotificationsGridDataSource notificationsGridDataSource;*/

    @Property
    private UsersSharedWithMeGridDataSource usersSharedWithMeGridDataSource;

    @Property
    private Measurement measurement;

    @Property
    private Measurement measurement2;

    @Property
    private UserNotification userNotification;

    @Property
    private UserProfile user;

    @Property
    private UserProfile selectedUser;


    @Property
    private String name;

    @Property
    private String username;

    @Property
    private Integer age;

    @Property
    private Integer height;

    @Property
    private Float weight;

    @Property
    private String gender;

    @Property
    private String lifeStye;

    @Property
    private MeasurementsStats measurementsStatsHOUR,measurementsStatsDAY,measurementsStatsWEEK,measurementsStatsMONTH,measurementsStatsYEAR;

    @Property
    private Date date1;

    @ActivationRequestParameter("date1")
    private String date1str;

    @Property
    private Date date2;

    @ActivationRequestParameter("date2")
    private String date2str;


    @Property
    @ActivationRequestParameter("dateFilter")
    private String dateFilter;



    @Property
    private Long usrId;

    @InjectComponent
    private Zone grid1Zone;

    @InjectComponent
    private Zone grid2Zone;

    @InjectComponent
    private Zone grid3Zone;

    @Inject
    private Request request;

    @Inject
    private AjaxResponseRenderer ajaxResponseRenderer;



    void onValidateFromDateFilterForm() {

        if (request.isXHR()) {
            ajaxResponseRenderer.addRender(grid1Zone);
        }
    }

    void onValidateFromFindMeasurementsForm(){
        date1str=getDateToString(date1);
        if (request.isXHR()) {
            ajaxResponseRenderer.addRender(grid2Zone);
        }
    }

    void onValidateFromFindNotificationsForm(){
        date2str=getDateToString(date2);
        if (request.isXHR()) {
            ajaxResponseRenderer.addRender(grid3Zone);
        }
    }






    public int getRowsPerPage() {
        return ROWS_PER_PAGE;
    }


    Object[] onPassivate(){
        return new Object[]{usrId};
    }

    void onActivate(Long userId) throws InstanceNotFoundException, ParseException {
        /*Se obtiene el id de usuario que se está inspeccionando.*/
        if(userId!=null){
            this.usrId=userId;
        }else{
            this.usrId=userSession.getUserProfileId();
        }
        /*Se obtiene su perfil y se establecen los parámetros que necesita la página*/
        UserProfile userProfile = userProfileService.findById(this.usrId);
        HealthStats healthStats = userProfile.getHealthStats();
        selectedUser=userProfile;
        name = userProfile.getName();
        username=userProfile.getUsername();
        age=healthStats.getAge();
        weight=healthStats.getWeight();
        height=healthStats.getHeight();
        gender=messages.get(healthStats.getGender().name());
        lifeStye=messages.get(healthStats.getLifeStyle().name());


        measurementsStatsHOUR=measurementService.getMeasurementsStats(this.usrId,MeasurementTimeRange.HOUR);
        measurementsStatsDAY=measurementService.getMeasurementsStats(this.usrId,MeasurementTimeRange.DAY);
        measurementsStatsWEEK=measurementService.getMeasurementsStats(this.usrId,MeasurementTimeRange.WEEK);
        measurementsStatsMONTH=measurementService.getMeasurementsStats(this.usrId,MeasurementTimeRange.MONTH);
        measurementsStatsYEAR=measurementService.getMeasurementsStats(this.usrId,MeasurementTimeRange.YEAR  );

        usersSharedWithMeGridDataSource=new UsersSharedWithMeGridDataSource(userProfileService,userSession.getUserProfileId());
    }


    void onPrepareForRender() throws InstanceNotFoundException {
        /*Por defecto la fecha será el día actual y el filtro será por hora.*/

        date1 = Calendar.getInstance().getTime();
        date1str=getDateToString(date1);


        date2 = Calendar.getInstance().getTime();
        date2str=getDateToString(date2);


        if(this.usrId==null){
            this.usrId=userSession.getUserProfileId();
        }

        dateFilter = MeasurementTimeRange.HOUR.name();
    }

    /*Devuelve los posibles valores del select box para el filtro del tiempo.*/
    public String getTimeRanges(){
        List<MeasurementTimeRange> timeRanges = Arrays.asList(MeasurementTimeRange.values());
        StringBuilder s = new StringBuilder();
        for(MeasurementTimeRange g: timeRanges){
            s.append(g.name()).append("=").append(messages.get(g.name())).append(",");
        }

        return s.substring(0,s.length()-1);
    }


    public DateFormat getDateFormat() {

        SimpleDateFormat dateFormat =
                (SimpleDateFormat) DateFormat.getDateInstance();
        dateFormat.applyPattern("dd/MM/yyyy  HH:mm:ss");

        return dateFormat;

    }

    /*Cuando se cambia la opción de usuario*/
    void onActionFromSelect(Long usrId) {
        this.usrId=usrId;
    }

    /*Cuando se borra un usuario de la lista de usuarios compartidos conmigo.*/
    void onActionFromDelete(Long usrId) throws InstanceNotFoundException {
        userProfileService.cancelShareProfileWith(usrId,userSession.getUserProfileId());
        if (request.isXHR()) {
            ajaxResponseRenderer.addRender(grid3Zone);
        }
    }

    private Date stringToDate(String date) throws  ParseException {
        return getDateFormatter().parse(date);
    }


    private String getDateToString(Date date) {
        if(date!=null) {
            return getDateFormatter().format(date);
        }else{
            return null;
        }
    }


    private DateFormat getDateFormatter() {
        SimpleDateFormat dateFormat =
                (SimpleDateFormat) DateFormat.getDateInstance();
        dateFormat.applyPattern("dd-MM-yyyy hh");

        return dateFormat;
    }

    /*Devuelve el título de una notificación. Se guardan en el properties.*/
    public String getNotificationTitle(UserNotification userNotification){
       return messages.get(userNotification.getNotification().getNotificationName()+"_TITLE");
    }

    /*Devuelve el cuerpo de una notificación. Se guardan en el properties.*/
    public String getNotificationBody(UserNotification userNotification){
        String body= messages.get(userNotification.getNotification().getNotificationName()+"_BODY");
        String[] split = userNotification.getNotificationValues().split(" ");
        for(int i=0;i<split.length;i++){
            int aux=i+1;
            body = body.replace("val"+aux,split[i]);
        }
        return body;
    }

    public GridDataSource getMeasurementsByFilter(){
        MeasurementTimeRange timeRange=MeasurementTimeRange.valueOf(dateFilter);
        return new MeasurementGridDataSource(measurementService,this.usrId,timeRange);
    }

    public GridDataSource getMeasurementsByDate() throws ParseException {
        Date a=stringToDate(date1str);
        return new MeasurementsByDateDataSource(measurementService,this.usrId,a);
    }

    public GridDataSource getNotificationsByDate() throws ParseException {
        Date a=stringToDate(date2str);
        return new NotificationsGridDataSource(notificationService,this.usrId,a);
    }


    public JSONObject getOptions() {
        return new JSONObject();
    }

    public boolean isMainUser(){
        return userSession.getUserProfileId().equals(user.getUsrId());
    }
}