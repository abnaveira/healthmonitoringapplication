package es.udc.tfg.healthmonitoringapplication.webservice.util;

import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.services.measurementservice.MeasurementService;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/*Esta clase (y el resto de clases similares) se usan para crear las tablas que muestran información
 * para ello se implementa GridDataSource.*/
public class MeasurementsByDateDataSource implements GridDataSource {
    private MeasurementService measurementService;
    private long usrId;
    private List<Measurement> measurements;
    private int startIndex;
    private Calendar date;


    public MeasurementsByDateDataSource(MeasurementService measurementService, long usrId, Date date) {
        this.measurementService = measurementService;
        this.usrId = usrId;
        this.date = Calendar.getInstance();
        this.date.setTime(date);
    }

    /*Devolvemos el número de filas totales*/
    @Override
    public int getAvailableRows() {
        return measurementService.getNumberOfMeasurementsByDate(usrId,date);
    }

    @Override
    public void prepare(int i, int i1, List<SortConstraint> list) {
        measurements=measurementService.getMeasurementsByUserIdByDate(usrId,date,i,i1-i+1);
        this.startIndex=i;

    }

    @Override
    public Object getRowValue(int i) {
        return measurements.get(i-this.startIndex);
    }

    @Override
    public Class<Measurement> getRowType() {
        return Measurement.class;
    }
}
