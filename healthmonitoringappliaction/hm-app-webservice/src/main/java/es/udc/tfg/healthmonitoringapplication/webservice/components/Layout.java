package es.udc.tfg.healthmonitoringapplication.webservice.components;

import es.udc.tfg.healthmonitoringapplication.webservice.pages.Index;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicy;
import es.udc.tfg.healthmonitoringapplication.webservice.services.AuthenticationPolicyType;
import es.udc.tfg.healthmonitoringapplication.webservice.util.CookiesManager;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UserSession;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Cookies;

/*Esta clase es la que establece la plantilla que establece la información que comparten todas las páginas*/
public class Layout {

    @Property
    @Parameter(required = true, defaultPrefix = "message")
    private String title;

    @Parameter(defaultPrefix = "literal")
    private Boolean showTitleInBody;

    @Property
    @SessionState(create=false)
    private UserSession userSession;

    @Inject
    private Cookies cookies;

    public boolean getShowTitleInBody() {

        if (showTitleInBody == null) {
            return true;
        } else {
            return showTitleInBody;
        }

    }

    @AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
    /*Al 'desloguearse' se elimina el userSession y las cookies y se vuelve a la página principal.*/
    Object onActionFromLogout() {
        userSession = null;
        CookiesManager.removeCookies(cookies);
        return Index.class;
    }
}