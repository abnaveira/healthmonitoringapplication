package es.udc.tfg.healthmonitoringapplication.webservice.services;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.exceptions.AuthenticationException;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.webservice.util.CookiesManager;
import es.udc.tfg.healthmonitoringapplication.webservice.util.UserSession;
import org.apache.tapestry5.services.*;

import java.io.IOException;

public class SessionFilter implements RequestFilter{

    private ApplicationStateManager applicationStateManager;
    private Cookies cookies;
    private UserProfileService userProfileService;

    public SessionFilter(ApplicationStateManager applicationStateManager,
                         Cookies cookies, UserProfileService userProfileService) {

        this.applicationStateManager = applicationStateManager;
        this.cookies = cookies;
        this.userProfileService = userProfileService;

    }

    public boolean service(Request request, Response response,
                           RequestHandler handler) throws IOException {

        if (!applicationStateManager.exists(UserSession.class)) {

            String loginName = CookiesManager.getLoginName(cookies);
            if (loginName != null) {

                String encryptedPassword = CookiesManager
                        .getEncryptedPassword(cookies);
                if (encryptedPassword != null) {

                    try {

                        UserProfile userProfile = userProfileService.login(loginName,
                                encryptedPassword, true);
                        UserSession userSession = new UserSession();
                        userSession.setUserProfileId(userProfile
                                .getUsrId());
                        userSession.setName(userProfile.getName());
                        applicationStateManager.set(UserSession.class,
                                userSession);

                    } catch (AuthenticationException e) {
                        CookiesManager.removeCookies(cookies);
                    }

                }

            }

        }

        handler.service(request, response);

        return true;
    }
}
