package es.udc.tfg.healthmonitoringapplication.restservice.security.util;

import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.AuthenticationTokenDetails;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;

public class TokenBasedSecurityContext implements SecurityContext {

    private AuthenticatedUserDetails authenticatedUserDetails;
    private AuthenticationTokenDetails authenticationTokenDetails;
    private final boolean secure;

    public TokenBasedSecurityContext(AuthenticatedUserDetails authenticatedUserDetails, AuthenticationTokenDetails authenticationTokenDetails, boolean secure) {
        this.authenticatedUserDetails = authenticatedUserDetails;
        this.authenticationTokenDetails = authenticationTokenDetails;
        this.secure = secure;

    }

    @Override
    public Principal getUserPrincipal() {
        return authenticatedUserDetails;
    }

    @Override
    public boolean isUserInRole(String s) {
        return authenticatedUserDetails.getAuthorities().contains(s);
    }

    @Override
    public boolean isSecure() {
        return secure;
    }

    @Override
    public String getAuthenticationScheme() {
        return "Bearer";
    }

    public AuthenticationTokenDetails getAuthenticationTokenDetails() {
        return authenticationTokenDetails;
    }
}
