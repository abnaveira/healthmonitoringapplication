package es.udc.tfg.healthmonitoringapplication.dtos;

import es.udc.tfg.healthmonitoringapplication.model.daos.notification.NotificationAlertLevel;

import java.util.Calendar;

/*Dto para el objeto UserNotification*/
public class UserNotificationDto {
    private String name;
    private String notificationValues;
    private Integer alertLevel;
    private Calendar date;

    public UserNotificationDto() {
    }

    public UserNotificationDto(String name,String notificationValues, NotificationAlertLevel alertLevel, Calendar date) {
        this.name = name;
        this.notificationValues=notificationValues;
        this.alertLevel=alertLevel.ordinal();
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAlertLevel() {
        return alertLevel;
    }

    public void setAlertLevel(NotificationAlertLevel alertLevel) {
        this.alertLevel = alertLevel.ordinal();
    }

    public String getNotificationValues() {
        return notificationValues;
    }

    public void setNotificationValues(String notificationValues) {
        this.notificationValues = notificationValues;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
}
