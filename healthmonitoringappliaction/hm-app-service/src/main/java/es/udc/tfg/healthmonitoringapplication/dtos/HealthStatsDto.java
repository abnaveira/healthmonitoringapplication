package es.udc.tfg.healthmonitoringapplication.dtos;

/*Dto para enviar informacion de HealthStats, el género y estilo de vida también son dtos*/
public class HealthStatsDto {
    private Integer age;
    private Integer height;
    private Float weight;
    private EnumDto gender;
    private EnumDto lifeStyle;

    public HealthStatsDto() {
    }

    public HealthStatsDto(Integer age, Integer height, Float weight,EnumDto gender, EnumDto lifeStyle) {
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender=gender;
        this.lifeStyle = lifeStyle;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public EnumDto getGender() {
        return gender;
    }

    public void setGender(EnumDto gender) {
        this.gender = gender;
    }

    public EnumDto getLifeStyle() {
        return lifeStyle;
    }

    public void setLifeStyle(EnumDto lifeStyle) {
        this.lifeStyle = lifeStyle;
    }
}
