package es.udc.tfg.healthmonitoringapplication.restservice.security;


import es.udc.tfg.healthmonitoringapplication.dtos.UserOptionsDto;
import es.udc.tfg.healthmonitoringapplication.dtos.UserProfileDto;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.healthstatsservice.HealthStatsService;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.util.HealthStatsToHealthStatsDto;
import es.udc.tfg.healthmonitoringapplication.util.ToUserOptionsDto;
import es.udc.tfg.healthmonitoringapplication.util.UserProfileToUserProfileDto;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.annotation.security.PermitAll;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;

@PermitAll
@RequestScoped
@Path("/users")
public class UserManagementEndpoint extends SpringBeanAutowiringSupport {

    //NO SE PORQUE NO RESUELVE BIEN ESTO
    @Value("${authentication.userDefaultRole:USER}")
    private String userDefaultRole;

    @Context
    SecurityContext securityContext;

    @Autowired
    RoleService roleService;

    @Autowired
    UserProfileService userProfileService;

    @Autowired
    HealthStatsService healthStatsService;

    /*Registro de un usuario*/
    @PermitAll
    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerUser(UserProfileDto dto) {
        try {
            /*Se creará con el rol de USER*/
            Role role = roleService.findByRoleName("USER");
            Set<Role> roles = new HashSet<>();
            roles.add(role);
            /*Se consigue la información de la petición y se crea el usuario*/
            UserProfile up = UserProfileToUserProfileDto.toUserProfile(dto, roles);
            HealthStats hs = HealthStatsToHealthStatsDto.toHealthStats(dto.getHealthStats());
            up.setHealthStats(hs);
            hs.setUserProfile(up);
            UserProfile userProfile = userProfileService.createUserProfile(up);
            /*Se genera el token*/
            String token = userProfileService.issueToken(userProfile.getUsername(), userProfileService.getRolesIdFromUserId(userProfile.getUsrId()));
            /*Se devuelve el token*/
            return Response.ok(token).build();
        } catch (InstanceNotFoundException e){
            return Response.status(Response.Status.NOT_FOUND).build();
        }catch (DuplicateInstanceException e){
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
        catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /*Devuelve las opciones disponibles a la hora de registrar o crear o actualizar un perfil*/
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response getUserRegisterOptions() {
            UserOptionsDto userOptionsDto = ToUserOptionsDto.toUserOptionsDto(Arrays.asList(LifeStyle.values()),Arrays.asList(Gender.values()));
            return Response.status(Response.Status.OK).entity(userOptionsDto).build();
    }

    /*Devuelve la información del usuario dado*/
    @GET
    @Path("me")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"USER","ADMIN"})
    public Response getAuthenticatedUser() {

        Principal principal = securityContext.getUserPrincipal();

        if (principal == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        try {
            UserProfile userProfile = userProfileService.findByUsername(principal.getName());
            UserProfileDto userProfileDto = UserProfileToUserProfileDto.toUserProfileDto(userProfile);
            return Response.ok(userProfileDto).build();
        }catch (InstanceNotFoundException e){
            return Response.status(Response.Status.NOT_FOUND).build();
        }catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /*Actualiza el perfil de un usuario dado*/
    @POST
    @Path("me")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"USER","ADMIN"})
    public Response updateUserProfile(UserProfileDto userProfileDto) {

        Principal principal = securityContext.getUserPrincipal();
        if (principal == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        try {
            UserProfile userProfile = userProfileService.findByUsername(principal.getName());
            Set<Role> roles = userProfile.getRoles();
            UserProfile userProfile1 = UserProfileToUserProfileDto.toUserProfile(userProfileDto,roles);
            userProfile1.setUsrId(userProfile.getUsrId());
            HealthStats hs = HealthStatsToHealthStatsDto.toHealthStats(userProfileDto.getHealthStats());
            hs.setUsrId(userProfile.getUsrId());
            userProfileService.updateUserProfile(userProfile1);
            healthStatsService.createOrUpdateHealthStats(hs);
            return Response.ok().build();
        }catch (InstanceNotFoundException e){
            return Response.status(Response.Status.NOT_FOUND).build();
        }catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
