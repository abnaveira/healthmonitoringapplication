package es.udc.tfg.healthmonitoringapplication.util;

import es.udc.tfg.healthmonitoringapplication.dtos.UserProfileDto;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;

import java.util.Set;

/*Clase para convertir de UserProfile a UserProfileDto y viceversa*/
public class UserProfileToUserProfileDto {


    public static UserProfile toUserProfile(UserProfileDto userProfileDto, Set<Role> roles){
        return new UserProfile(userProfileDto.getName(),userProfileDto.getUsername(),userProfileDto.getPassword(),roles);
    }

    public static UserProfileDto toUserProfileDto(UserProfile userProfile){
        return new UserProfileDto(userProfile.getName(),userProfile.getUsername(),userProfile.getPassword(),HealthStatsToHealthStatsDto.toHealthStatsDto(userProfile.getHealthStats()));
    }

}
