package es.udc.tfg.healthmonitoringapplication.util;

import es.udc.tfg.healthmonitoringapplication.dtos.MeasurementDto;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;

import java.util.ArrayList;
import java.util.List;

/*Clase para convertir de Measurement a MeasurementDto y viceversa*/
public class MeasurementToMeasurementDtoConversor {

    public static Measurement toMeasurement(MeasurementDto measurementDto, UserProfile userProfile){
        Measurement m=new Measurement(measurementDto.getBandId(),measurementDto.getHeartRate(),measurementDto.getSteps(),
                measurementDto.getCalories(),measurementDto.getDistance(),measurementDto.getDate());
        m.setUserProfile(userProfile);
        return m;
    }

    public static MeasurementDto toMeasurementDto(Measurement measurement){
        return new MeasurementDto(measurement.getBandId(),measurement.getHeartRate(),
                measurement.getSteps(),measurement.getCalories(),measurement.getDistance(),measurement.getDate());
    }

    public static List<MeasurementDto> toMeasurementDtoList(List<Measurement> measurementList){
        List<MeasurementDto> measurementDtoList = new ArrayList<>();
        for(Measurement m : measurementList){
            measurementDtoList.add(toMeasurementDto(m));
        }
        return measurementDtoList;
    }
}
