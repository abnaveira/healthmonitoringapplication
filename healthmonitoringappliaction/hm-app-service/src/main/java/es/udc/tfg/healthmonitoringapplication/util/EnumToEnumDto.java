package es.udc.tfg.healthmonitoringapplication.util;

import es.udc.tfg.healthmonitoringapplication.dtos.EnumDto;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;

import java.util.ArrayList;
import java.util.List;

/*Clase para convertir de Enum a EnumDto y viceversa*/

public class EnumToEnumDto {

    public static EnumDto toEnumDto(LifeStyle lifeStyle){
        return new EnumDto(lifeStyle.ordinal(),lifeStyle.name());
    }

    public static EnumDto toEnumDto(Gender gender){
        return new EnumDto(gender.ordinal(),gender.name());
    }

    public static List<EnumDto> toEnumDtoLifeStyleList(List<LifeStyle> lifeStyleList){
        List<EnumDto> list=new ArrayList<>();
        for(LifeStyle lifeStyle : lifeStyleList){
            list.add(toEnumDto(lifeStyle));
        }
        return list;
    }

    public static List<EnumDto> toEnumDtoGenderList(List<Gender> genderList){
        List<EnumDto> list=new ArrayList<>();
        for(Gender gender : genderList){
            list.add(toEnumDto(gender));
        }
        return list;
    }

    public static LifeStyle toLifeStyle(EnumDto lifeStyle){
        return LifeStyle.valueOf(lifeStyle.getName());
    }

    public static Gender toGender(EnumDto gender){
        return Gender.valueOf(gender.getName());
    }

}
