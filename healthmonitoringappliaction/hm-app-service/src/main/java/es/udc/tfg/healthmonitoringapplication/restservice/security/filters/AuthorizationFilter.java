package es.udc.tfg.healthmonitoringapplication.restservice.security.filters;

import es.udc.tfg.healthmonitoringapplication.model.exceptions.AccessDeniedException;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;


/*Se ejecuta antes de las peticiones al endpoint para ver que rol requiere el usuario, o si se requiere estar autenticado*/
@Provider
@Priority(Priorities.AUTHORIZATION)
public class AuthorizationFilter implements ContainerRequestFilter {
    @Context
    private ResourceInfo resourceInfo;

    /*En esta clase cogemos buscamos en las anotaciones que habrá sobre el método para ver el nivel de autorización requerido*/
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        Method method = resourceInfo.getResourceMethod();

        /*Si aparece la anotación DenyAll no se podrá llamar al metodo*/
        // @DenyAll on the method takes precedence over @RolesAllowed and @PermitAll
        if (method.isAnnotationPresent(DenyAll.class)) {
            throw new AccessDeniedException("You don't have permissions to perform this action.");
        }

        /*Si se especifican ciertos roles se comprueba que el usuario que hace la petición tiene ese rol*/
        // @RolesAllowed on the method takes precedence over @PermitAll
        RolesAllowed rolesAllowed = method.getAnnotation(RolesAllowed.class);
        if (rolesAllowed != null) {
            performAuthorization(rolesAllowed.value(), requestContext);
            return;
        }

        /*Cualquier persona puede acceder a ese método.*/
        // @PermitAll on the method takes precedence over @RolesAllowed on the class
        if (method.isAnnotationPresent(PermitAll.class)) {
            // Do nothing
            return;
        }

        // @DenyAll can't be attached to classes
        /*Se hacen las mismas comprobaciones para las clases, ya que se pueden anotar tanto métodos como clases.*/
        // @RolesAllowed on the class takes precedence over @PermitAll on the class
        rolesAllowed = resourceInfo.getResourceClass().getAnnotation(RolesAllowed.class);
        if (rolesAllowed != null) {
            performAuthorization(rolesAllowed.value(), requestContext);
        }

        // @PermitAll on the class
        if (resourceInfo.getResourceClass().isAnnotationPresent(PermitAll.class)) {
            // Do nothing
            return;
        }

        // Se requiere estar autenticado para los métodos no anotados
        if (!isAuthenticated(requestContext)) {
            throw new AccessDeniedException("Authentication is required to perform this action.");
        }

    }

    private void performAuthorization(String[] rolesAllowed, ContainerRequestContext requestContext) {

        /*Se comprueba que el usuario está autenticado*/
        if (rolesAllowed.length > 0 && !isAuthenticated(requestContext)) {
            throw new AccessDeniedException("Authentication is required to perform this action.");
        }

        /*Se comprueban que el usuario contiene algún rol de los requeridos por el método o clase. */
        for (final String role : rolesAllowed) {
            if (requestContext.getSecurityContext().isUserInRole(role)) {
                return;
            }
        }
        /*Si no se lanza una excepción*/
        throw new AccessDeniedException("You don't have permissions to perform this action.");
    }


    private boolean isAuthenticated(final ContainerRequestContext requestContext) {
        return requestContext.getSecurityContext().getUserPrincipal() != null;
    }
}
