package es.udc.tfg.healthmonitoringapplication.util;

import es.udc.tfg.healthmonitoringapplication.dtos.UserOptionsDto;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;

import java.util.List;

import static es.udc.tfg.healthmonitoringapplication.util.EnumToEnumDto.*;

/*Clase para crear OptionsDto (no hay una clase equivalente en el modelo)*/
public class ToUserOptionsDto {

    public static UserOptionsDto toUserOptionsDto(List<LifeStyle> lifeStyleList,List<Gender> gender){
        return new UserOptionsDto(toEnumDtoLifeStyleList(lifeStyleList),toEnumDtoGenderList(gender));
    }



}
