package es.udc.tfg.healthmonitoringapplication.restservice;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/*Configuración del servicio. Empezará siempre con la ruta /rest/... */
@ApplicationPath("rest")
public class Configuration extends Application {
}
