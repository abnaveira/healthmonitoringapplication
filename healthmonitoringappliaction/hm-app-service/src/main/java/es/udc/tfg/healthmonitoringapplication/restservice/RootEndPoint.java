package es.udc.tfg.healthmonitoringapplication.restservice;

import es.udc.tfg.healthmonitoringapplication.model.services.measurementservice.MeasurementService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice.MonitoringServiceImpl;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@PermitAll
@RequestScoped
@Path("/")

public class RootEndPoint extends SpringBeanAutowiringSupport {
    @Autowired
    MonitoringServiceImpl testComponent;

    @Autowired
    UserProfileService userProfileService;

    @Autowired
    MeasurementService measurementService;

    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserRegisterOptions(){
        return Response.ok().build();
    }
}
