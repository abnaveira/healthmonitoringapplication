package es.udc.tfg.healthmonitoringapplication.util;

import es.udc.tfg.healthmonitoringapplication.dtos.HealthStatsDto;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;

/*Clase para convertir de HealthStats a HealthStatsDto y viceversa*/
public class HealthStatsToHealthStatsDto {

    public static HealthStats toHealthStats(HealthStatsDto healthStatsDto){
        return new HealthStats(healthStatsDto.getAge(),healthStatsDto.getHeight(),healthStatsDto.getWeight(),EnumToEnumDto.toGender(healthStatsDto.getGender()), EnumToEnumDto.toLifeStyle(healthStatsDto.getLifeStyle()));
    }

    public static HealthStatsDto toHealthStatsDto (HealthStats healthStats){
        return new HealthStatsDto(healthStats.getAge(),healthStats.getHeight(),healthStats.getWeight(),EnumToEnumDto.toEnumDto(healthStats.getGender()), EnumToEnumDto.toEnumDto(healthStats.getLifeStyle()));
    }

}
