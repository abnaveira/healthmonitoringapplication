package es.udc.tfg.healthmonitoringapplication.restservice.security;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.exceptions.AuthenticationException;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.PasswordEncoder;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;


@RequestScoped
@Path ("/authentication")
public class AuthenticationEndpoint extends SpringBeanAutowiringSupport {

    @Context
    SecurityContext securityContext;

    @Autowired
    UserProfileService userProfileService;


    /*Autenticación de usuario*/
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @PermitAll
    public Response authenticateUser(@FormParam("username") String username,
                                     @FormParam("password") String password){
        try {
            /*Se busca el usuario*/
            UserProfile userProfile = userProfileService.login(username, password,false);
            /*Se genera el token*/
            String token = userProfileService.issueToken(username, userProfileService.getRolesIdFromUserId(userProfile.getUsrId()));
            /*Se devuelve el token*/
            return Response.ok(token).build();

        }catch (AuthenticationException e){
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }catch (Exception e){
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }


}
