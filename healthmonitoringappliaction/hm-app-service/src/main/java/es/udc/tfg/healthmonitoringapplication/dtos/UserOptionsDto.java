package es.udc.tfg.healthmonitoringapplication.dtos;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;

import java.util.List;


/*Este dto se usa para mostrar las diferentes opciones a la hora de crear o actualizar un perfil*/
public class UserOptionsDto {

    private List<EnumDto> lifeStyle;
    private List<EnumDto> gender;

    public UserOptionsDto() {
    }

    public UserOptionsDto(List<EnumDto> lifeStyle, List<EnumDto> gender) {
        this.lifeStyle = lifeStyle;
        this.gender=gender;
    }

    public List<EnumDto> getLifeStyle() {
        return lifeStyle;
    }

    public void setLifeStyle(List<EnumDto> lifeStyle) {
        this.lifeStyle = lifeStyle;
    }

    public List<EnumDto> getGender() {
        return gender;
    }

    public void setGender(List<EnumDto> gender) {
        this.gender = gender;
    }
}
