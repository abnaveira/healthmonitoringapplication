package es.udc.tfg.healthmonitoringapplication.dtos;

/*Dto para enviar Enums ya que la conversión automática solo envía su valor numérico.*/
public class EnumDto {
    private int id;
    private String name;

    public EnumDto() {
    }

    public EnumDto(int id, String name) {
        this.id = id;
        this.name=name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setNae(String name) {
        this.name = name;
    }
}
