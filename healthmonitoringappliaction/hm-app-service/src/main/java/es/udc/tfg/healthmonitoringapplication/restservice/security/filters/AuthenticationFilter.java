package es.udc.tfg.healthmonitoringapplication.restservice.security.filters;

import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.exceptions.InvalidAuthenticationTokenException;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.AuthenticationTokenDetails;
import es.udc.tfg.healthmonitoringapplication.restservice.security.util.AuthenticatedUserDetails;
import es.udc.tfg.healthmonitoringapplication.restservice.security.util.TokenBasedSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;

/*Se ejecuta antes que las llamadas a los métodos del endpoint para comprobar que el token (si se envía) es correcto.*/
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        /*Se accede a la parte de autenticación de la cabezera HTTP*/
        String authorizationHeader = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        /*Se extrae el token y se comprueba que todo esté correcto*/
        if(authorizationHeader!=null && authorizationHeader.startsWith("Bearer ")){
            String authenticationToken = authorizationHeader.substring(7);
            handleTokenBasedAuthentication(authenticationToken,containerRequestContext);
        }
    }

    private void handleTokenBasedAuthentication(String authenticationToken, ContainerRequestContext containerRequestContext) {

        try(ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(SPRING_CONFIG_FILE)) {
            /*Se accede al servicio*/
            UserProfileService userProfileService= ctx.getBean(UserProfileService.class);
            /*Se parsea el token*/
            AuthenticationTokenDetails authenticationTokenDetails=userProfileService.parseToken(authenticationToken);
            /*Se busca el usuario*/
            UserProfile userProfile = userProfileService.findByUsername(authenticationTokenDetails.getUsername());
            Set<Role> roles = userProfile.getRoles();
            Set<String>strRoles=new HashSet<>();
            for(Role r : roles){
                strRoles.add(r.getRoleName());
            }
            /*Se crea un objeto AuthenticatedUserDetails que contiene el nombre de usuario y los nombres de sus roles*/
            AuthenticatedUserDetails authenticatedUserDetails= new AuthenticatedUserDetails(userProfile.getUsername(),strRoles);
            boolean isSecure = containerRequestContext.getSecurityContext().isSecure();
            /*La clase que implementa ContainerRequestContext se llama TokenBasedSecurityContext y está en util.*/
            SecurityContext securityContext = new TokenBasedSecurityContext(authenticatedUserDetails, authenticationTokenDetails, isSecure);
            containerRequestContext.setSecurityContext(securityContext);

        }catch (InvalidAuthenticationTokenException e){
            Response response = Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
            throw new WebApplicationException(response);
        }catch (Exception e){
            throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
        }
    }


}
