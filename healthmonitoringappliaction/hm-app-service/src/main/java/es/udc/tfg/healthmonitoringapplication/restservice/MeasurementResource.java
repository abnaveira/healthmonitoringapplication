package es.udc.tfg.healthmonitoringapplication.restservice;

import es.udc.tfg.healthmonitoringapplication.dtos.MeasurementDto;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.measurementservice.MeasurementService;

import es.udc.tfg.healthmonitoringapplication.model.services.notificationservice.NotificationService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice.MonitoringService;
import es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice.MonitoringServiceImpl;
import es.udc.tfg.healthmonitoringapplication.util.MeasurementToMeasurementDtoConversor;
import es.udc.tfg.healthmonitoringapplication.util.UserNotificationToUserNotificationDto;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.*;

@RequestScoped
@Path("/measurements")
public class MeasurementResource extends SpringBeanAutowiringSupport {
    @Context
    SecurityContext securityContext;

    @Autowired
    MeasurementService measurementService;

    @Autowired
    UserProfileService userProfileService;

    @Autowired
    MonitoringService monitoringService;

    @Autowired
    NotificationService notificationService;

    /*Añade una nueva medida al usuario que realize la petición.
    * Aparte de añadir una medida en BD también la añade al sistema de reglas.
    * Además despues de ejecutar las reglas, devolverá las reglas que están sin leer.*/
    @POST
    @RolesAllowed({"USER","ADMIN"})
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMeasurement(MeasurementDto m){
        try {
            Principal principal = securityContext.getUserPrincipal();
            String username = principal.getName();
            UserProfile userProfile = userProfileService.findByUsername(username);
            //Es mejor poner la fecha con la que la medida llega al servidor, sobre todo para el sistema de reglas.
            m.setDate(Calendar.getInstance());
            Measurement newMeasurement = MeasurementToMeasurementDtoConversor.toMeasurement(m,userProfile);
            measurementService.createMeasurement(newMeasurement);
            monitoringService.insertMeasurement(newMeasurement);
            List<UserNotification> userNotifications = notificationService.getUnreadUserNotifications(userProfile.getUsrId(),true);
            return Response.status(Response.Status.OK).entity(UserNotificationToUserNotificationDto.toUserNotificationDtoList(userNotifications)).build();
        }catch (NullPointerException e){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        catch (Exception e){
            return  Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /*Devuelve todas las medidas aplicando todos los filtros posibles. Como máximo la cantidad de elementos sería
    * 12+24+7+30+12=85=> 12 medidas para hora, 24 para día, 7 para semana, 30 para mes y 12 para año. Y obviamente casi nunca se
    * llegaría a ese valor ya que para eso tendría que estar midiendo continuamente todos los días del año, en todo momento.
    */
    @GET
    @RolesAllowed({"USER","ADMIN"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMeasurementsByRange(){
        try {
            Principal principal = securityContext.getUserPrincipal();
            String username = principal.getName();
            UserProfile userProfile = userProfileService.findByUsername(username);

            List<List<Measurement>> list= measurementService.getMeasurementsInAllRangesById(userProfile.getUsrId());
            List<List<MeasurementDto>> list1= new ArrayList<>();
            for(List<Measurement> l : list){
                list1.add(MeasurementToMeasurementDtoConversor.toMeasurementDtoList(l));
            }
            return Response.ok(list1).build();
        }catch (Exception e){
            e.printStackTrace();
            return  Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
