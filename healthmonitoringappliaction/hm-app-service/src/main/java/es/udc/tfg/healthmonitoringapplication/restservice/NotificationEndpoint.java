package es.udc.tfg.healthmonitoringapplication.restservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.notificationservice.NotificationService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.util.UserNotificationToUserNotificationDto;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.List;

@RequestScoped
@Path("/notifications")
public class NotificationEndpoint extends SpringBeanAutowiringSupport {
    @Context
    SecurityContext securityContext;

    @Autowired
    NotificationService notificationService;

    @Autowired
    UserProfileService userProfileService;
    /*Devuelve todas las notificaciones que no han sido leídas por el usuario. MeasurementResource también las devuelve al insertar una medida.*/
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"USER","ADMIN"})
    public Response getNotifications() {
        try {
            Principal principal = securityContext.getUserPrincipal();
            String username = principal.getName();
            UserProfile userProfile = userProfileService.findByUsername(username);
            List<UserNotification> userNotifications = notificationService.getUnreadUserNotifications(userProfile.getUsrId(),true);
            return Response.status(Response.Status.OK).entity(UserNotificationToUserNotificationDto.toUserNotificationDtoList(userNotifications)).build();
        }catch (InstanceNotFoundException e){
            e.printStackTrace();
            return  Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
