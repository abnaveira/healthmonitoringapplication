package es.udc.tfg.healthmonitoringapplication.restservice.security.util;

import java.security.Principal;
import java.util.Collections;
import java.util.Set;

public class AuthenticatedUserDetails implements Principal {
    private final String username;
    private final Set<String>authorities;

    public AuthenticatedUserDetails(String username, Set<String> authorities) {
        this.username = username;
        this.authorities = Collections.unmodifiableSet(authorities);
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    @Override
    public String getName() {
        return username;
    }
}
