package es.udc.tfg.healthmonitoringapplication.util;

import es.udc.tfg.healthmonitoringapplication.dtos.UserNotificationDto;
import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;

import java.util.ArrayList;
import java.util.List;

/*Clase para convertir de UserNotification a UserNotificationDto y viceversa*/
public class UserNotificationToUserNotificationDto {
    public static UserNotificationDto toUserNotificationDto(UserNotification userNotification){
        return new UserNotificationDto(userNotification.getNotification().getNotificationName(),userNotification.getNotificationValues(),userNotification.getNotification().getAlertLevel(),userNotification.getDate());
    }

    public static List<UserNotificationDto> toUserNotificationDtoList(List<UserNotification> userNotifications){
        List<UserNotificationDto> list=new ArrayList<>();
        for(UserNotification userNotification:userNotifications){
            list.add(toUserNotificationDto(userNotification));
        }
        return list;
    }
}
