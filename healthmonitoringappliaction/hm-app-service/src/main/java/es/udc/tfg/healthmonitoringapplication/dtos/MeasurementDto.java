package es.udc.tfg.healthmonitoringapplication.dtos;

import java.util.Calendar;

/*Dto para el objeto Measurement*/
public class MeasurementDto {
    private String bandId;
    private Integer heartRate;
    private Long steps;
    private Long calories;
    private Long distance;
    private Calendar date;

    public MeasurementDto() {
    }

    public MeasurementDto(String bandId, Integer heartRate, Long steps, Long calories, Long distance, Calendar date) {
        this.bandId = bandId;
        this.heartRate = heartRate;
        this.steps = steps;
        this.calories = calories;
        this.distance = distance;
        this.date = date;
    }

    public String getBandId() {
        return bandId;
    }

    public void setBandId(String bandId) {
        this.bandId = bandId;
    }

    public Integer getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(Integer heartRate) {
        this.heartRate = heartRate;
    }

    public Long getSteps() {
        return steps;
    }

    public void setSteps(Long steps) {
        this.steps = steps;
    }

    public Long getCalories() {
        return calories;
    }

    public void setCalories(Long calories) {
        this.calories = calories;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
}
