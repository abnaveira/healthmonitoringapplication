package es.udc.tfg.healthmonitoringapplication.dtos;


/*Este dto es el correspondiente a UserProfile*/
public class UserProfileDto {
    private String name;
    private String username;
    private String password;
    private HealthStatsDto healthStats;

    public UserProfileDto() {
    }

    public UserProfileDto(String name, String username, String password, HealthStatsDto healthStats) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.healthStats = healthStats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public HealthStatsDto getHealthStats() {
        return healthStats;
    }

    public void setHealthStats(HealthStatsDto healthStats) {
        this.healthStats = healthStats;
    }
}
