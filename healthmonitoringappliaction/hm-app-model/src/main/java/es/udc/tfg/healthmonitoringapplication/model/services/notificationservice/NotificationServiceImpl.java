package es.udc.tfg.healthmonitoringapplication.model.services.notificationservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.notification.Notification;
import es.udc.tfg.healthmonitoringapplication.model.daos.notification.NotificationDao;
import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotificationDao;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfileDao;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.List;

@Service("notificationService")
@Transactional
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    NotificationDao notificationDao;

    @Autowired
    UserProfileDao userProfileDao;

    @Autowired
    UserNotificationDao userNotificationDao;

    @Override
    public void addNewNotificationByName(String notificationName,String notificationValues, long usrId, Calendar date) throws InstanceNotFoundException {
        /*Se busca la notificacion*/
        Notification notificationByName = notificationDao.findNotificationByName(notificationName);
        /*Se busca el usuario*/
        UserProfile userProfile = userProfileDao.find(usrId);
        /*Se añade la notificación.*/
        UserNotification userNotification = new UserNotification(userProfile,notificationValues,notificationByName,date,false);
        userNotificationDao.save(userNotification);
    }

    @Override
    public List<UserNotification> getUnreadUserNotifications(long usrId,boolean markAsViewed) {
        List<UserNotification> userNotifications = userNotificationDao.getUnreadUserNotification(usrId);
        /*Si se quiren marcar como leídas.*/
        if(markAsViewed){
            for(UserNotification u: userNotifications){
                u.setViewed(true);
                userNotificationDao.save(u);
            }
        }
        return userNotifications;
    }

    @Override
    public List<UserNotification> findUserNotifications(long usrId, Calendar date, int startIndex, int count) {
        return userNotificationDao.findUserNotifications(usrId,date,startIndex,count);
    }

    @Override
    public int getNumberOfNotifications(long usrId, Calendar date) {
        return userNotificationDao.getNumberOfNotifications(usrId,date);
    }

}
