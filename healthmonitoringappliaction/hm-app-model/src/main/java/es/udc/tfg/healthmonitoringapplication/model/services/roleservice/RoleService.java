package es.udc.tfg.healthmonitoringapplication.model.services.roleservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

import java.util.List;

public interface RoleService {

    /*Se busca un rol por su nombre*/
    Role findByRoleName(String roleName) throws InstanceNotFoundException;

    /*Se añade un rol al usuario*/
    void addRoleToUser(Long usrId,String roleName) throws InstanceNotFoundException;

    /*Se borra un rol al usuario*/
    void deleteRoleFromUser(Long usrId,String roleName) throws InstanceNotFoundException;
}
