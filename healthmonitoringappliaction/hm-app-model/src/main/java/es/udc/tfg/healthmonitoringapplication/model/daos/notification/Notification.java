package es.udc.tfg.healthmonitoringapplication.model.daos.notification;

import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="Notification")
public class Notification {
    /*
    * Las notificaciones se insertan automaticamente desde el script MySQLPopulateTables.sql
    * */
    private Long notificationId;/*id de la notificación*/
    private String notificationName;/*nombre de la notificación*/
    private NotificationAlertLevel alertLevel;/*nivel de alerta de la notificación.*/
    private Set<UserNotification> userNotifications;/*Relaciona usuarios con notificaciones.*/

    public Notification() {
    }

    public Notification(String notificationName,NotificationAlertLevel alertLevel) {
        this.notificationName = notificationName;
        this.alertLevel=alertLevel;
    }
    @Column(name = "notificationId")
    @SequenceGenerator(
            name="NotificationIdGenerator",
            sequenceName = "NotificationSequence"
    )
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,
            generator = "NotificationIdGenerator")
    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationName() {
        return notificationName;
    }

    public void setNotificationName(String notificationName) {
        this.notificationName = notificationName;
    }


    @OneToMany(mappedBy = "notification",fetch = FetchType.LAZY)
    public Set<UserNotification> getUserNotifications() {
        return userNotifications;
    }

    public void setUserNotifications(Set<UserNotification> userNotifications) {
        this.userNotifications = userNotifications;
    }
    @Enumerated(EnumType.ORDINAL)
    public NotificationAlertLevel getAlertLevel() {
        return alertLevel;
    }

    public void setAlertLevel(NotificationAlertLevel alertLevel) {
        this.alertLevel = alertLevel;
    }
}
