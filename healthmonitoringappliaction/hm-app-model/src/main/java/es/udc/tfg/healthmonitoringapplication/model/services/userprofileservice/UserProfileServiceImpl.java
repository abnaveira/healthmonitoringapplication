package es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.exceptions.AuthenticationException;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfileDao;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.AuthenticationTokenDetails;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.AuthenticationTokenIssuer;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.AuthenticationTokenParser;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.PasswordEncoder;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements  UserProfileService {
    @Autowired
    UserProfileDao userProfileDao;

    /*Límite de refrescos que tiene el token. Se almacena en application.properties en resources*/
    @Value("${authentication.jwt.refreshLimit}")
    private Integer refreshLimit;

    /*Duración máxima del token.Se almacena en application.properties en resources */
    @Value("${authentication.jwt.validFor}")
    private Long validFor;


    @Autowired
    AuthenticationTokenIssuer tokenIssuer;

    @Autowired
    AuthenticationTokenParser tokenParser;

    @Override
    public UserProfile createUserProfile(UserProfile userProfile) throws DuplicateInstanceException {
        try{
            userProfileDao.findUserByUsername(userProfile.getUsername());
            throw new DuplicateInstanceException("User already exists. ",userProfile.getUsername(),UserProfile.class.getName());
        }catch (InstanceNotFoundException e){
            //hasheamos la contraseña antes de almacenarla
            PasswordEncoder passwordEncoder=new PasswordEncoder();
            userProfile.setPassword(passwordEncoder.hashPassword(userProfile.getPassword()));
            userProfileDao.save(userProfile);
            return userProfile;
        }
    }

    @Override
    public UserProfile updateUserProfile(UserProfile userProfile) throws InstanceNotFoundException {
        /*se busca a ver si existe*/
        userProfileDao.find(userProfile.getUsrId());
        userProfileDao.save(userProfile);
        return userProfile;
    }

    @Override
    public UserProfile findById(Long id) throws InstanceNotFoundException {
        return userProfileDao.find(id);
    }

    @Override
    public UserProfile findByUsername(String username) throws InstanceNotFoundException {
        return userProfileDao.findUserByUsername(username);
    }

    @Override
    public UserProfile login(String username, String password, boolean encrypted) throws AuthenticationException {
        try {
            UserProfile userProfile = userProfileDao.findUserByUsername(username);
            if(!encrypted) {
                /*si se envía la contraseña en plano se se convierte para comparar con el valor almacenado*/
                PasswordEncoder passwordEncoder = new PasswordEncoder();
                if (!passwordEncoder.checkPassword(password, userProfile.getPassword())) {
                    throw new AuthenticationException("Incorrect Password.");
                }
            }else {
                /*si se envía encriptada simplemente se compara*/
                if (!userProfile.getPassword().equals(password)) {
                    throw new AuthenticationException("Incorrect Password.");
                }
            }
            return userProfile;
        }catch (InstanceNotFoundException e){
            throw new AuthenticationException("User not found");
        }
    }

    @Override
    public String issueToken(String username, Set<Long> authorities) {
        String id = generateTokenIdentifier();
        ZonedDateTime issuedDate = ZonedDateTime.now();
        ZonedDateTime expirationDate = calculateExpirationDate(issuedDate);
        /*Se inicializa el token. No se almacena el id de usuario, sino que el username.*/
        AuthenticationTokenDetails authenticationTokenDetails = new AuthenticationTokenDetails.
                Builder().withId(id).withUsername(username).withIssuedDate(issuedDate).
                withExpirationDate(expirationDate).withRefreshCount(0).withRefreshLimit(refreshLimit).
                withAuthorities(authorities).build();

        return tokenIssuer.issueToken(authenticationTokenDetails);
    }

    /*Se saca la información del token*/
    @Override
    public AuthenticationTokenDetails parseToken(String token) {
        return tokenParser.parseToken(token);
    }

    /*Se devuelven los ids de los roles de un usuario. NO los roles como objetos*/
    @Override
    public Set<Long> getRolesIdFromUserId(Long usrId) throws InstanceNotFoundException {
        UserProfile userProfile = userProfileDao.find(usrId);
        Set<Long> roles = new HashSet<>();
        for(Role r : userProfile.getRoles()){
            roles.add(r.getRoleId());
        }
        return roles;
    }

    @Override
    public void changePassword(Long usrId, String oldPassword, String newPassword) throws InstanceNotFoundException {
        UserProfile userProfile = userProfileDao.find(usrId);
        PasswordEncoder passwordEncoder = new PasswordEncoder();
        /*Se comprueba que las contraseñas coinciden*/
        if (!passwordEncoder.checkPassword(oldPassword, userProfile.getPassword())) {
            throw new AuthenticationException("Incorrect Password.");
        }
        /*Se hashea e inserta la nueva contraseña*/
        userProfile.setPassword(passwordEncoder.hashPassword(newPassword));
        userProfileDao.save(userProfile);
    }


    @Override
    public List<UserProfile> getUserProfilesSharedWithMe(Long usrId,int startIndex,int count) throws InstanceNotFoundException {
        return userProfileDao.getUserProfilesSharedWithMe(usrId,startIndex,count);
    }

    @Override
    public List<UserProfile> getUserProfilesISharedWith(Long usrId,int startIndex,int count) throws InstanceNotFoundException {
        return userProfileDao.getUserProfilesISharedWith(usrId,startIndex,count);
    }

    @Override
    public List<UserProfile> getUsersByKeyword(String keyword, int startIndex, int count) {
        return userProfileDao.findUsersByKeyword(keyword,startIndex,count);
    }

    @Override
    public int getNumberOfUsersByKeyword(String keyword) {
        return userProfileDao.getNumberOfUsersByKeyword(keyword);
    }

    @Override
    public void shareProfileWith(Long usrId, Long shareWith) throws InstanceNotFoundException {
        userProfileDao.shareProfileWith(usrId,shareWith);
    }

    @Override
    public void cancelShareProfileWith(Long usrId, Long shareWith) throws InstanceNotFoundException {
        userProfileDao.cancelShareProfileWith(usrId,shareWith);
    }

    @Override
    public int getNumberOfUsersSharedWithMe(Long usrId) {
        return userProfileDao.getNumberOfUsersSharedWithMe(usrId);
    }

    @Override
    public int getNumberOfUsersISharedWith(Long usrId) {
        return userProfileDao.getNumberOfUsersISharedWith(usrId);
    }

    /*Se calcula la fecha exacta de expiración del token*/
    private ZonedDateTime calculateExpirationDate(ZonedDateTime issuedDate) {
        return issuedDate.plusSeconds(validFor);
    }

    /*Se genera el UUID del token*/
    private String generateTokenIdentifier() {
        return UUID.randomUUID().toString();
    }
}
