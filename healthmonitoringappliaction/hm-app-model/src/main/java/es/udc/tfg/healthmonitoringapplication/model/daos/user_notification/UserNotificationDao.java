package es.udc.tfg.healthmonitoringapplication.model.daos.user_notification;

import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDao;

import java.util.Calendar;
import java.util.List;

public interface UserNotificationDao extends GenericDao<UserNotification,Long> {

    /*Devuelve las notificaciones no leídas por el usuario*/
    List<UserNotification> getUnreadUserNotification(long usrId);

    /*Devuelve todas las notificaciones (leídas o no) de un usuario un día dado*/
    List<UserNotification> findUserNotifications(long usrId, Calendar date, int startIndex, int count);

    /*Devuelve el número de notificaciones para findUserNotifications. Se usa para paginación en un grid*/
    int getNumberOfNotifications(long usrId, Calendar date);
}
