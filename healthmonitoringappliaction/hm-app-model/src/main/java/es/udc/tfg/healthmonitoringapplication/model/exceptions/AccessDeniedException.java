package es.udc.tfg.healthmonitoringapplication.model.exceptions;

public class AccessDeniedException extends RuntimeException {

    /*Excepción cuando no se permite realizar una operación*/
    public AccessDeniedException(String message) {
        super(message);
    }

    /*public AccessDeniedException(String message, Throwable cause) {
        super(message, cause);
    }*/
}
