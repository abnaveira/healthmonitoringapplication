package es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util;

import es.udc.tfg.healthmonitoringapplication.model.exceptions.AuthenticationException;
import org.mindrot.jbcrypt.BCrypt;

public class PasswordEncoder {

    /*Hashea la contraseña*/
    public String hashPassword(String plainTextPassword) {
        String salt = BCrypt.gensalt();
        String hash= BCrypt.hashpw(plainTextPassword, salt);
        return hash;
    }

    /*Se comprueba que la contraseña en plano es igual que la hasheada.*/
    public boolean checkPassword(String plainTextPassword, String hashedPassword) {

        if (null == hashedPassword || !hashedPassword.startsWith("$2a$")) {
            throw new AuthenticationException("Incorrect Password.");
        }

        return BCrypt.checkpw(plainTextPassword, hashedPassword);
    }
}
