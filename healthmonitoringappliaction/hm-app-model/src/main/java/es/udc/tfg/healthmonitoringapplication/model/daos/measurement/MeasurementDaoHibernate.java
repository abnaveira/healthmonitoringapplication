package es.udc.tfg.healthmonitoringapplication.model.daos.measurement;

import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDaoHibernate;
import org.springframework.stereotype.Repository;

import java.awt.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Repository("measurementDao")
public class MeasurementDaoHibernate extends GenericDaoHibernate<Measurement,Long> implements MeasurementDao {


    @Override
    public List<Measurement> getMeasurementInRangeByUserId(Long usrId, MeasurementTimeRange timeRange) {
        List<Measurement> list1= new ArrayList<>();
        /*si no se pasa un filtro se devuelve la lista vacía*/
        if(timeRange==null){
            return  list1;
        }else {
            String q=getQuery(timeRange);

                List<Object[]> list = getSession().createSQLQuery(q).setParameter("usrId", usrId).list();
                /*Necesitamos crear el objeto Measurement manualmente ya que se campos como el bandId y la fecha impiden agrupar automaticamente.*/
                for (Object[] row : list) {
                    Calendar calendar = getTime(timeRange, (String) row[0]);
                    list1.add(new Measurement(null, ((BigDecimal) row[2]).intValue(),
                            ((BigDecimal) row[3]).longValue(),
                            ((BigDecimal) row[4]).longValue(),
                            ((BigDecimal) row[5]).longValue(), calendar));
                }
        }
        return list1;
    }



    @Override
    public List<Measurement> getMeasurementInRangeByUserIdWithPag(Long usrId, MeasurementTimeRange timeRange, int startIndex, int count) {
        List<Measurement> list1= new ArrayList<>();
        /*si no se pasa un filtro se devuelve la lista vacía*/
        if(timeRange==null){
            return  list1;
        }else {
            String q = getQuery(timeRange);
            List<Object[]> list = getSession().createSQLQuery(q).setParameter("usrId", usrId).setFirstResult(startIndex).setMaxResults(count).list();
            /*Necesitamos crear el objeto Measurement manualmente ya que se campos como el bandId y la fecha impiden agrupar automaticamente.*/
            for (Object[] row : list) {
                Calendar calendar = getTime(timeRange, (String) row[0]);
                list1.add(new Measurement(null, ((BigDecimal) row[2]).intValue(),
                        ((BigDecimal) row[3]).longValue(),
                        ((BigDecimal) row[4]).longValue(),
                        ((BigDecimal) row[5]).longValue(), calendar));
            }
        }
        return list1;
    }

    @Override
    public List<Measurement> getMeasurementsByUserIdByDate(Long usrId, Calendar date, int startIndex, int count) {
        List<Measurement> list=(List<Measurement>) getSession().createQuery("SELECT m FROM Measurement m WHERE DAY(date) = DAY(:d) AND userProfile.usrId=:usrId").setParameter("d",date).setParameter("usrId",usrId).setFirstResult(startIndex).setMaxResults(count).getResultList();
        return list;
    }

    @Override
    public int getNumberOfMeasurements(Long usrId,MeasurementTimeRange timeRange) {
        String q = getQueryCount(timeRange);
       BigInteger i = (BigInteger) getSession().createSQLQuery(q).setParameter("usrId",usrId).getSingleResult();
        return  i.intValue();
    }

    @Override
    public int getNumberOfMeasurementsByDate(Long usrId, Calendar date) {
        long i=(Long) getSession().createQuery("SELECT COUNT(m) FROM Measurement m WHERE DAY(date) = DAY(:d) AND userProfile.usrId=:usrId").setParameter("d",date).setParameter("usrId",usrId).getSingleResult();

        return (int) i;
    }

    @Override
    public MeasurementsStats getMeasurementsStats(Long usrId, MeasurementTimeRange timeRange) {
        String q = getQueryStats(timeRange);
        List<Object[]> list=getSession().createSQLQuery(q).setParameter("usrId",usrId).list();
        Object[] object = list.get(0);
        Float meanHr= object[0]==null?null:((BigDecimal) object[0]).floatValue();
        Float deviationHr= object[1]==null?null:((Double) object[1]).floatValue();
        Float peakHr= object[2]==null?null:((Integer) object[2]).floatValue();
        Float lowHr= object[3]==null?null:((Integer) object[3]).floatValue();

        return new MeasurementsStats(meanHr,deviationHr,peakHr,lowHr);
    }


    /*Dependiendo del filtro la query es diferente ya que tenemos que agrupar el tiempo de forma distinta.*/
    private String getQueryStats(MeasurementTimeRange timeRange){
        String q="";
        if(timeRange==MeasurementTimeRange.HOUR){
            q = "SELECT  avg(heartRate), std(heartRate), max(heartRate), min(heartRate) FROM Measurement WHERE date>=date_format(NOW(),'%Y-%m-%d-%H') AND usrId=:usrId";
        }else if (timeRange == MeasurementTimeRange.DAY) {
            q = "SELECT  avg(heartRate), std(heartRate), max(heartRate), min(heartRate) FROM Measurement WHERE date>=date_format(NOW(),'%Y-%m-%d') AND usrId=:usrId";
        } else if (timeRange == MeasurementTimeRange.WEEK) {
            q = "SELECT  avg(heartRate), std(heartRate), max(heartRate), min(heartRate) FROM Measurement WHERE WEEK(date)=WEEK(current_date) AND YEAR(date)=YEAR(current_date) AND usrId=:usrId";
        }else if(timeRange==MeasurementTimeRange.MONTH){
            q = "SELECT  avg(heartRate), std(heartRate), max(heartRate), min(heartRate) FROM Measurement WHERE MONTH (date)=MONTH (current_date) AND YEAR(date)=YEAR(current_date) AND usrId=:usrId";
        }else if(timeRange==MeasurementTimeRange.YEAR){
            q = "SELECT  avg(heartRate), std(heartRate), max(heartRate), min(heartRate) FROM Measurement WHERE YEAR(date)=YEAR(current_date) AND usrId=:usrId";
        }
        return q;
    }

    /*Dependiendo del filtro la query es diferente ya que tenemos que agrupar el tiempo de forma distinta.*/
    private String getQueryCount(MeasurementTimeRange timeRange){
        String q="";
        if(timeRange==MeasurementTimeRange.HOUR){
            q = "SELECT count(*) FROM (SELECT  date_format(date,'%Y-%m-%d-%H-%i') as groupTime, usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance) FROM Measurement WHERE date>=date_format(NOW(),'%Y-%m-%d-%H') AND usrId=:usrId GROUP BY groupTime) a";
        }else if (timeRange == MeasurementTimeRange.DAY) {
            q = "SELECT count(*) FROM (SELECT  date_format(date,'%Y-%m-%d-%H') as groupTime, usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance) FROM Measurement WHERE date>=date_format(NOW(),'%Y-%m-%d') AND usrId=:usrId GROUP BY groupTime) a";
        } else if (timeRange == MeasurementTimeRange.WEEK) {
            q = "SELECT count(*) FROM (SELECT  date_format(date,'%Y-%m-%d')  as a , usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance)FROM Measurement WHERE WEEK(date)=WEEK(current_date) AND YEAR(date)=YEAR(current_date) AND usrId=:usrId GROUP BY a) a";
        }else if(timeRange==MeasurementTimeRange.MONTH){
            q = "SELECT count(*) FROM (SELECT date_format(date,'%Y-%m-%d') as a, usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance) FROM Measurement WHERE MONTH (date)=MONTH (current_date) AND YEAR(date)=YEAR(current_date) AND usrId=:usrId GROUP BY a) a";
        }else if(timeRange==MeasurementTimeRange.YEAR){
            q = "SELECT count(*) FROM (SELECT date_format(date,'%Y-%m') as a , usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance) FROM Measurement WHERE YEAR(date)=YEAR(current_date) AND usrId=:usrId GROUP BY a) a";
        }
        return q;
    }

    /*Dependiendo del filtro la query es diferente ya que tenemos que agrupar el tiempo de forma distinta.*/
    private String getQuery(MeasurementTimeRange timeRange){
        String q="";
        if(timeRange==MeasurementTimeRange.HOUR){
            q = "SELECT  date_format(date,'%Y-%m-%d-%H-%i') as groupTime, usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance) FROM Measurement WHERE date>=date_format(NOW(),'%Y-%m-%d-%H') AND usrId=:usrId GROUP BY groupTime";
        }else if (timeRange == MeasurementTimeRange.DAY) {
            q = "SELECT  date_format(date,'%Y-%m-%d-%H') as groupTime, usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance) FROM Measurement WHERE date>=date_format(NOW(),'%Y-%m-%d') AND usrId=:usrId GROUP BY groupTime";
        } else if (timeRange == MeasurementTimeRange.WEEK) {
            q = "SELECT  date_format(date,'%Y-%m-%d')  as a , usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance)FROM Measurement WHERE WEEK(date)=WEEK(current_date) AND YEAR(date)=YEAR(current_date) AND usrId=:usrId GROUP BY a";
        }else if(timeRange==MeasurementTimeRange.MONTH){
            q = "SELECT date_format(date,'%Y-%m-%d') as a, usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance) FROM Measurement WHERE MONTH (date)=MONTH (current_date) AND YEAR(date)=YEAR(current_date) AND usrId=:usrId GROUP BY a";
        }else if(timeRange==MeasurementTimeRange.YEAR){
            q = "SELECT date_format(date,'%Y-%m') as a , usrId, round(avg(heartRate),0), avg(steps), avg(calories), avg(distance) FROM Measurement WHERE YEAR(date)=YEAR(current_date) AND usrId=:usrId GROUP BY a";
        }
        return q;
    }

    /*Dependiendo del filtro debemos parsear la fecha de forma distinta.*/
    private Calendar getTime(MeasurementTimeRange timeRange,String value){
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        if(timeRange==MeasurementTimeRange.DAY){
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH");
        }else if(timeRange==MeasurementTimeRange.WEEK || timeRange==MeasurementTimeRange.MONTH){
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        }else if(timeRange==MeasurementTimeRange.YEAR){
            simpleDateFormat= new SimpleDateFormat("yyyy-MM");
        }
        try {
            calendar.setTime(simpleDateFormat.parse(value));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }
}
