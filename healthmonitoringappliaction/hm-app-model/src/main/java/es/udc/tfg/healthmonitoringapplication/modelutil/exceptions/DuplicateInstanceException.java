package es.udc.tfg.healthmonitoringapplication.modelutil.exceptions;

/*Excepción para objetos duplicados*/
public class DuplicateInstanceException extends Exception {
    private Object key;
    private String className;

    public DuplicateInstanceException(String message,Object key, String className) {
        super(message +" - "+key);
        this.key=key;
        this.className=className;
    }

    public Object getKey() {
        return key;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
