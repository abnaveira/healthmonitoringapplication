package es.udc.tfg.healthmonitoringapplication.modelutil.exceptions;

/*Excepción cuando no existe una instancia.*/
public class InstanceNotFoundException extends InstanceException{
    public InstanceNotFoundException(Object key, String className) {
        super("Instance not found", key, className);
    }

}
