package es.udc.tfg.healthmonitoringapplication.model.daos.healthstats;

public enum LifeStyle {
    /*
    * Los 5 estilos de vida posibles, se mapean directamente en BD
    * */
    LOW_EXERCISE,LIGHT_EXERCISE,MEDIUM_EXERCISE,HIGH_EXERCISE,STRONG_EXERCISE;

}
