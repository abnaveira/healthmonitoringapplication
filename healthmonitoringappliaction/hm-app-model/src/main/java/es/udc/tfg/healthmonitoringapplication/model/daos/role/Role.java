package es.udc.tfg.healthmonitoringapplication.model.daos.role;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Role {
    /*Rol que tiene el usuario, pej: USER, ADMIN.
    * Se pueden tener varios. Los roles se insertan en MySQLPopulateTables*/
    private Long roleId;/*id del rol*/
    private String roleName;/*nombre del rol*/
    private Set<UserProfile> userProfiles;/*Usuarios que tienen el rol*/
    public Role() {
    }

    public Role(Long roleId, String roleName) {
        this.roleId = roleId;
        this.roleName = roleName;
    }

    @Column(name="roleId")
    @SequenceGenerator(
            name="AuthIdGenerator",
            sequenceName = "AuthSequence"
    )
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,
                    generator = "AuthIdGenerator"
    )
    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long authId) {
        this.roleId = authId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String authName) {
        this.roleName = authName;
    }

    @ManyToMany(cascade = {CascadeType.ALL},mappedBy = "roles")
    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }
}
