package es.udc.tfg.healthmonitoringapplication.model.daos.user_notification;


import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDaoHibernate;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.List;

@Repository("userNotificationDao")
public class UserNotificationDaoHibernate extends GenericDaoHibernate<UserNotification,Long> implements UserNotificationDao {

    /*Devuelve las notificaciones no leídas por el usuario*/
    @Override
    public List<UserNotification> getUnreadUserNotification(long usrId) {
        List<UserNotification> usrId1 = (List<UserNotification>) getSession().createQuery("SELECT u FROM UserNotification u WHERE userProfile.usrId = :usrId AND viewed=false").setParameter("usrId", usrId).list();
        return usrId1;
    }

    /*Devuelve todas las notificaciones (leídas o no) de un usuario un día dado*/
    @Override
    public List<UserNotification> findUserNotifications(long usrId, Calendar date, int startIndex, int count) {
        List<UserNotification> list = (List<UserNotification>) getSession().createQuery("SELECT u FROM UserNotification u WHERE userProfile.usrId = :usrId AND DAY(date)=DAY(:d)").
                setParameter("usrId", usrId).setParameter("d",date).setFirstResult(startIndex).setMaxResults(count).getResultList();
        return list;
    }

    /*Devuelve el número de notificaciones para findUserNotifications. Se usa para paginación en un grid*/
    @Override
    public int getNumberOfNotifications(long usrId, Calendar date) {
        long i = (Long) getSession().createQuery("SELECT COUNT(u) FROM UserNotification u WHERE userProfile.usrId = :usrId AND DAY(date)=DAY(:d)").
                setParameter("usrId", usrId).setParameter("d",date).getSingleResult();
        return (int) i;
    }
}
