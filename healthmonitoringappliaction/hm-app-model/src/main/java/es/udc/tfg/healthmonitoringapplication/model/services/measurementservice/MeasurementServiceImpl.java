package es.udc.tfg.healthmonitoringapplication.model.services.measurementservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementDao;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementTimeRange;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementsStats;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
@Service("measurementService")
@Transactional
public class MeasurementServiceImpl implements MeasurementService {

    @Autowired
    private MeasurementDao measurementDao;


    public Measurement createMeasurement(Measurement measurement) {
        measurementDao.save(measurement);
        return measurement;
    }

    public Measurement findMeasurement(Long id) throws InstanceNotFoundException {
        return measurementDao.find(id);
    }

    @Override
    public List<Measurement> getMeasurementsInRangeById(Long usrId, MeasurementTimeRange timeRange,int startIndex,int count) {
        return measurementDao.getMeasurementInRangeByUserIdWithPag(usrId,timeRange,startIndex,count);

    }

    /*Se podría llamar varias veces getMeasurementsInRangeById, pero así se simplifica en el cliente*/
    @Override
    public List<List<Measurement>> getMeasurementsInAllRangesById(Long usrId) {
        List<List<Measurement>>lists = new ArrayList<>();
        lists.add(measurementDao.getMeasurementInRangeByUserId(usrId,MeasurementTimeRange.HOUR));
        lists.add(measurementDao.getMeasurementInRangeByUserId(usrId,MeasurementTimeRange.DAY));
        lists.add(measurementDao.getMeasurementInRangeByUserId(usrId,MeasurementTimeRange.WEEK));
        lists.add(measurementDao.getMeasurementInRangeByUserId(usrId,MeasurementTimeRange.MONTH));
        lists.add(measurementDao.getMeasurementInRangeByUserId(usrId,MeasurementTimeRange.YEAR));
        return lists;
    }

    @Override
    public List<Measurement> getMeasurementsByUserIdByDate(Long usrId, Calendar date,int startIndex, int count) {
        return measurementDao.getMeasurementsByUserIdByDate(usrId,date,startIndex,count);
    }

    @Override
    public int getNumberOfMeasurements(long usrId,MeasurementTimeRange timeRange) {
        return measurementDao.getNumberOfMeasurements(usrId,timeRange);
    }

    @Override
    public MeasurementsStats getMeasurementsStats(Long usrId, MeasurementTimeRange timeRange) {
        return measurementDao.getMeasurementsStats(usrId,timeRange);
    }

    @Override
    public int getNumberOfMeasurementsByDate(Long usrId, Calendar date) {
        return measurementDao.getNumberOfMeasurementsByDate(usrId,date);
    }


}
