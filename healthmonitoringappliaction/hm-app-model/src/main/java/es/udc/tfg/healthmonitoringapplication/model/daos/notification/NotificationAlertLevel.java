package es.udc.tfg.healthmonitoringapplication.model.daos.notification;

public enum NotificationAlertLevel {
    /*Clasificación del nivel de peligro de la notificación.*/
    LOW,MEDIUM,HIGH,CRITICAL
}
