package es.udc.tfg.healthmonitoringapplication.model.daos.healthstats;

import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDaoHibernate;
import org.springframework.stereotype.Repository;

@Repository("healthStatsDao")
public class HealthStatsHibernate extends GenericDaoHibernate<HealthStats,Long> implements HealthStatsDao {

}
