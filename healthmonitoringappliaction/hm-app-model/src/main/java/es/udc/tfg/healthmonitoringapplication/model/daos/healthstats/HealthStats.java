package es.udc.tfg.healthmonitoringapplication.model.daos.healthstats;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

@Entity
@Table(name = "HealthStats")
public class HealthStats {
    private Long usrId;
    private Integer age;/*Edad del usuario*/
    private Integer height;/*Altura del usuario*/
    private Float weight;/*Peso  del usuario*/
    private Gender gender;/*Género del usuario, se mapea directamente el enum con la columna de la BD*/
    private LifeStyle lifeStyle;/*Estilo de vida */
    private UserProfile userProfile;/*Perfil del usuario*/

    public HealthStats() {
    }

    public HealthStats(Integer age, Integer height, Float weight,Gender gender, LifeStyle lifeStyle) {
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender=gender;
        this.lifeStyle = lifeStyle;
    }

    /*El id es el mismo que el id de UserProfile ya que se mapea 1 a 1*/
    @Id
    @GeneratedValue(generator = "foreignGen")
    @GenericGenerator(strategy = "foreign", name="foreignGen",
            parameters = @Parameter(name = "property", value="userProfile"))
    @Column(name = "usrId")
    public Long getUsrId() {
        return usrId;
    }

    public void setUsrId(Long usrId) {
        this.usrId = usrId;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    /*Se mapea directamente al enum*/
    @Enumerated(EnumType.ORDINAL)
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /*Se mapea directamente al enum*/
    @Enumerated(EnumType.ORDINAL)
    public LifeStyle getLifeStyle() {
        return lifeStyle;
    }

    public void setLifeStyle(LifeStyle lifeStyle) {
        this.lifeStyle = lifeStyle;
    }

    @OneToOne(mappedBy = "healthStats")
    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
