package es.udc.tfg.healthmonitoringapplication.model.daos.measurement;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;

import javax.persistence.*;
import java.util.Calendar;

@Entity
public class Measurement {
    private Long measurementId;/*id de la medida*/
    private String bandId;/*identificador de la pulsera*/
    private Integer heartRate;/*valor de las pulsaciones*/
    private Long steps;/*valor de los pasos*/
    private Long calories;/*valor de las calorías*/
    private Long distance;/*valor de la distancia recorrida*/
    private Calendar date;/*fecha de creación de la medida*/
    private UserProfile userProfile;/*usuario al que está asociado la medida*/

    public Measurement() {
    }

    public Measurement(String bandId, Integer heartRate, Long steps, Long calories, Long distance, Calendar date) {
        this.bandId = bandId;
        this.heartRate = heartRate;
        this.steps = steps;
        this.calories = calories;
        this.distance = distance;
        this.date = date;
    }

    @Column(name = "measurementId")
    @SequenceGenerator(
            name = "MeasurementIdGenerator",
            sequenceName="MeasurementSequence"
    )
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,
                    generator = "MeasurementIdGenerator")
    public Long getMeasurementId(){return this.measurementId;}
    public void setMeasurementId(Long measurementId){this.measurementId=measurementId;}

    public String getBandId() {
        return bandId;
    }

    public void setBandId(String bandId) {
        this.bandId = bandId;
    }

    public Integer getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(Integer heartRate) {
        this.heartRate = heartRate;
    }

    public Long getSteps() {
        return steps;
    }

    public void setSteps(Long steps) {
        this.steps = steps;
    }

    public Long getCalories() {
        return calories;
    }

    public void setCalories(Long calories) {
        this.calories = calories;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    @ManyToOne
    @JoinColumn(name = "usrId")
    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
