package es.udc.tfg.healthmonitoringapplication.model.daos.notification;

import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDao;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;


public interface NotificationDao extends GenericDao<Notification,Long> {
    /*Busca el nombre de una notificación por su nombre*/
    public Notification findNotificationByName(String name) throws InstanceNotFoundException;
}
