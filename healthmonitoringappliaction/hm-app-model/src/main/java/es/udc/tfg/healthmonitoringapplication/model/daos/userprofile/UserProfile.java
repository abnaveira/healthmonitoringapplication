package es.udc.tfg.healthmonitoringapplication.model.daos.userprofile;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="UserProfile")
public class UserProfile{
    private Long usrId;/*Id del usuario; autogenerado*/
    private String name;/*Nombre*/
    private String username;/*Nombre de usuario*/
    private String password;/*Contraseña; se almacena un hash, no en plano*/
    private Set<Role> roles;/*Roles que tiene el usuario*/
    private Set<UserNotification> userNotifications;/*Notificaciones del usuario*/
    private HealthStats healthStats;/*Información del usuario, va 1 a 1*/
    private Set<UserProfile> sharing;/*Lista de usuarios con los que el usuario comparte su perfil*/
    private Set<UserProfile> sharedWithMe;/*Lista de usuarios que han compartido su usuario con este perfil*/

    public UserProfile() {
    }

    public UserProfile(String name, String username, String password, Set<Role> roles) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    @Column(name="usrId")
    @SequenceGenerator(
            name="UserProfileIdGenerator",
            sequenceName = "UserProfileSequence"
    )
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,
                    generator = "UserProfileIdGenerator")
    public Long getUsrId() {
        return usrId;
    }

    public void setUsrId(Long usrId) {
        this.usrId = usrId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name="User_Role", joinColumns = {@JoinColumn(name = "usrId")}, inverseJoinColumns = {@JoinColumn(name = "roleId")})
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "userProfile")
    public Set<UserNotification> getUserNotifications() {
        return userNotifications;
    }

    public void setUserNotifications(Set<UserNotification> userNotifications) {
        this.userNotifications = userNotifications;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    public HealthStats getHealthStats() {
        return healthStats;
    }

    public void setHealthStats(HealthStats healthStats) {
        this.healthStats = healthStats;
    }


    @ManyToMany
    @JoinTable(name="UserShare",
            joinColumns=@JoinColumn(name="pacient"),
            inverseJoinColumns=@JoinColumn(name="doctor"))
    public Set<UserProfile> getSharing() {
        return sharing;
    }

    public void setSharing(Set<UserProfile> sharing) {
        this.sharing = sharing;
    }

    @ManyToMany
    @JoinTable(name="UserShare",
            joinColumns=@JoinColumn(name="doctor"),
            inverseJoinColumns=@JoinColumn(name="pacient")
    )
    public Set<UserProfile> getSharedWithMe() {
        return sharedWithMe;
    }

    public void setSharedWithMe(Set<UserProfile> sharedWithMe) {
        this.sharedWithMe = sharedWithMe;
    }

}
