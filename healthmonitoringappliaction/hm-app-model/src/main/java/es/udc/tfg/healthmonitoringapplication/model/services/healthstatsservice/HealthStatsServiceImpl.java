package es.udc.tfg.healthmonitoringapplication.model.services.healthstatsservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStatsDao;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("healthStatsService")
@Transactional
public class HealthStatsServiceImpl implements HealthStatsService {

    /*Se necesita este dao*/
    @Autowired
    HealthStatsDao healthStatsDao;

    @Override
    public HealthStats createOrUpdateHealthStats(HealthStats healthStats) {
        healthStatsDao.save(healthStats);
        return healthStats;
    }
}
