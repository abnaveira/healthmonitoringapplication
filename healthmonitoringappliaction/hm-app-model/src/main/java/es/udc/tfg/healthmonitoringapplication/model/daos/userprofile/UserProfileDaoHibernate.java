package es.udc.tfg.healthmonitoringapplication.model.daos.userprofile;


import es.udc.tfg.healthmonitoringapplication.model.exceptions.AuthenticationException;
import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDaoHibernate;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.*;

@Repository("userProfileDao")
public class UserProfileDaoHibernate extends GenericDaoHibernate<UserProfile,Long> implements UserProfileDao {


    @Override
    public UserProfile findUserByUsername(String username) throws  InstanceNotFoundException {
        UserProfile user= (UserProfile) getSession().createQuery("SELECT u from UserProfile u WHERE username = :username")
                .setParameter("username",username).uniqueResult();
        if(user==null){
            throw new InstanceNotFoundException(username,UserProfile.class.getName());
        }
        return user;
    }

    @Override
    public List<UserProfile> getUserProfilesSharedWithMe(Long usrId,int startIndex,int count) throws InstanceNotFoundException {
        List<UserProfile> userProfiles=getSession().
                createQuery("SELECT u FROM UserProfile u WHERE u.usrId IN (SELECT fr.usrId from UserProfile u1 INNER JOIN u1.sharedWithMe fr WHERE u1.usrId=:usrId) OR u.usrId=:usrId2").
                setParameter("usrId",usrId).setParameter("usrId2",usrId).setFirstResult(startIndex).setMaxResults(count).getResultList();
        /*UserProfile userProfile = find(usrId);
        userProfiles.add(0,userProfile);*/
        return userProfiles;
    }

    @Override
    public List<UserProfile> getUserProfilesISharedWith(Long usrId,int startIndex,int count) throws InstanceNotFoundException {
        List<UserProfile> list = getSession().createQuery("SELECT u.sharing FROM UserProfile u WHERE u.usrId=:usrId").setParameter("usrId",usrId).setFirstResult(startIndex).setMaxResults(count).getResultList();
        return list;
    }

    @Override
    public void shareProfileWith(Long usrId, Long shareWith) throws InstanceNotFoundException {
        if(!usrId.equals(shareWith)) {
            UserProfile pacient = find(usrId);
            UserProfile doctor = find(shareWith);
            Set<UserProfile> sharing = pacient.getSharing();
            if(sharing==null){
                sharing=new HashSet<>();
                pacient.setSharing(sharing);
            }
            if (!sharing.contains(doctor)) {

                sharing.add(doctor);
               save(pacient);
            }
        }
    }

    @Override
    public void cancelShareProfileWith(Long usrId, Long shareWith) throws InstanceNotFoundException {
        if(!usrId.equals(shareWith)) {
            UserProfile pacient = find(usrId);
            UserProfile doctor = find(shareWith);
            Set<UserProfile> sharing = pacient.getSharing();
            if(sharing==null){
                sharing=new HashSet<>();
                pacient.setSharing(sharing);
            }
            if (sharing.contains(doctor)) {
                sharing.remove(doctor);
                save(pacient);
            }
        }
    }

    @Override
    public List<UserProfile> findUsersByKeyword(String keyword,int startIndex,int count) {
        List<UserProfile> users = getSession().createQuery("SELECT u FROM UserProfile u WHERE name LIKE CONCAT('%',:k,'%')").setParameter("k", keyword)
                .setFirstResult(startIndex).setMaxResults(count).getResultList();
        return users;
    }

    @Override
    public int getNumberOfUsersByKeyword(String keyword) {
       long i =(Long) getSession().createQuery("SELECT COUNT(u) FROM UserProfile u WHERE name LIKE CONCAT('%',:k,'%')").setParameter("k", keyword)
                .getSingleResult();
        return (int) i;
    }

    @Override
    public int getNumberOfUsersSharedWithMe(Long usrId){
        /*Se suma 1 para incluír al propio usuario*/
        int i =(Integer) getSession().createQuery("SELECT u.sharedWithMe.size FROM UserProfile u WHERE usrId=:usrId" ).setParameter("usrId",usrId).getSingleResult();
        return i+1;
    }

    @Override
    public int getNumberOfUsersISharedWith(Long usrId) {

        int i =(Integer) getSession().createQuery("SELECT u.sharing.size FROM UserProfile u WHERE usrId=:usrId" ).setParameter("usrId",usrId).getSingleResult();
        return i;
    }

}
