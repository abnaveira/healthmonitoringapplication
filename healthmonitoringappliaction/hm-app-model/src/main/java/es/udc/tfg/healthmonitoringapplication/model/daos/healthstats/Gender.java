package es.udc.tfg.healthmonitoringapplication.model.daos.healthstats;

public enum Gender {
    /*
    * Género del usuario,su valor es mapeado directamente en la BD.
    * */
    MALE,FEMALE;

}
