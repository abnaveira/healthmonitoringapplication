package es.udc.tfg.healthmonitoringapplication.model.daos.measurement;

import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDao;

import java.util.Calendar;
import java.util.List;

public interface MeasurementDao extends GenericDao<Measurement,Long> {

    /*Devuelve las medidas aplicando un filtro de tiempo, Los valores se calculan promediando, el valor de la pulsera será nulo.*/
    List<Measurement> getMeasurementInRangeByUserId(Long usrId,MeasurementTimeRange timeRange);

    /*Lo mismo que getMeasurementInRangeByUserId pero con paginación*/
    List<Measurement> getMeasurementInRangeByUserIdWithPag(Long usrId,MeasurementTimeRange timeRange,int startIndex,int count);

    /*Devuelve todas las medidas un día dado, con paginación. Se diferencia de getMeasurementInRangeByUserId en que no agrupa las mediciones,
    * sino que las devuelve todas.*/
    List<Measurement> getMeasurementsByUserIdByDate(Long usrId,Calendar date,int startIndex,int count);

    /*Devuelve la cantidad de resultados que tendrá la petición getMeasurementInRangeByUserIdWithPag, se usa para los grids*/
    int getNumberOfMeasurements(Long usrId,MeasurementTimeRange timeRange);

    /*Devuelve la cantidad de resultados que tendrá la petición getMeasurementsByUserIdByDate, se usa para los grids*/
    int getNumberOfMeasurementsByDate(Long usrId, Calendar date);

    /*Devuelve las estadísticas aplicando un filtro*/
    MeasurementsStats getMeasurementsStats(Long usrId,MeasurementTimeRange timeRange);

}
