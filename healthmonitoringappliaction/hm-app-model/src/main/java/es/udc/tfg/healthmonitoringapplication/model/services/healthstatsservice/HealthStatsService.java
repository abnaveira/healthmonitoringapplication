package es.udc.tfg.healthmonitoringapplication.model.services.healthstatsservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

import java.util.List;

public interface HealthStatsService {

    /*Actualiza las estadísticas de un usuario*/
    HealthStats createOrUpdateHealthStats(HealthStats healthStats);
}
