package es.udc.tfg.healthmonitoringapplication.model.services.roleservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.RoleDao;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfileDao;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service("roleService")
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleDao roleDao;

    @Autowired
    UserProfileDao userProfileDao;

    @Override
    public Role findByRoleName(String roleName) throws InstanceNotFoundException {
        return roleDao.findByRoleName(roleName);
    }

    @Override
    public void addRoleToUser(Long usrId, String roleName) throws InstanceNotFoundException {
        UserProfile userProfile = userProfileDao.find(usrId);
        Set<Role> roles = userProfile.getRoles();
        Role role = roleDao.findByRoleName(roleName);
        /*Si el usuario no contiene el rol se añade.*/
        if(!roles.contains(role)){
            roles.add(role);
        }
        userProfile.setRoles(roles);
        /*Se guardan los cambios.*/
        userProfileDao.save(userProfile);
    }

    @Override
    public void deleteRoleFromUser(Long usrId, String roleName) throws InstanceNotFoundException {
        UserProfile userProfile = userProfileDao.find(usrId);
        Set<Role> roles = userProfile.getRoles();
        Role role = roleDao.findByRoleName(roleName);
        /*Si el usuario contiene el rol, se elemina*/
        if(roles.contains(role)){
            roles.remove(role);
        }
        userProfile.setRoles(roles);
        /*Se guardan los cambios.*/
        userProfileDao.save(userProfile);
    }
}
