package es.udc.tfg.healthmonitoringapplication.model.daos.healthstats;


import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDao;

public interface HealthStatsDao extends GenericDao<HealthStats,Long> {

}
