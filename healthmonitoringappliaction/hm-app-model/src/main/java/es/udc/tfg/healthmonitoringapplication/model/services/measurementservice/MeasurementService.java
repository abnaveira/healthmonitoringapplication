package es.udc.tfg.healthmonitoringapplication.model.services.measurementservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementTimeRange;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementsStats;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

import java.util.Calendar;
import java.util.List;

public interface MeasurementService {

    /*Crea una nueva medida*/
    Measurement createMeasurement(Measurement measurement);

    /*Busca una medida por su identificador*/
    Measurement findMeasurement(Long id) throws InstanceNotFoundException;

    /*Busca una medida aplicando un filtro de tiempo, con paginación*/
    List<Measurement> getMeasurementsInRangeById(Long usrId, MeasurementTimeRange timeRange,int startIndex,int count);

    /*Devuelve el resultado de aplicar todos los filtros de tiempo, cada lista es el resultado de un filtro*/
    List<List<Measurement>> getMeasurementsInAllRangesById(Long usrId);

    /*Devuelve todas las medidas de un día dado. Con paginación.*/
    List<Measurement> getMeasurementsByUserIdByDate(Long usrId, Calendar date,int startIndex, int count);

    /*Devuelve el número de medidas aplicando un filtro. Con paginación*/
    int getNumberOfMeasurements(long usrId,MeasurementTimeRange timeRange);

    /*Devuelve las estadísticas aplicando un filtro de tiempo.*/
    MeasurementsStats getMeasurementsStats(Long usrId, MeasurementTimeRange timeRange);

    /*Devuelve el número de medidas en un día dado.*/
    int getNumberOfMeasurementsByDate(Long usrId, Calendar date);
}
