package es.udc.tfg.healthmonitoringapplication.model.daos.role;

import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDaoHibernate;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.springframework.stereotype.Repository;

@Repository("roleDao")
public class RoleDaoHibernate extends GenericDaoHibernate<Role,Long> implements RoleDao {

    /*Buscar un rol por nombre, si no se encuentra sale un InstanceNotFound*/
    @Override
    public Role findByRoleName(String roleName) throws InstanceNotFoundException {
        Role role =(Role) getSession().createQuery("SELECT r FROM Role r WHERE roleName = :roleName")
                .setParameter("roleName",roleName).uniqueResult();
        if(role==null){
            throw new InstanceNotFoundException(roleName,Role.class.getName());
        }
        return role;
    }
}
