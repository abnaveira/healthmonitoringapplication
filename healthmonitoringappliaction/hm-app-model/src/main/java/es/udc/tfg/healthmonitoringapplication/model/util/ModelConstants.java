package es.udc.tfg.healthmonitoringapplication.model.util;

public final class ModelConstants {
    /*Se usa en los tests para saber donde está el fichero de configuración.*/
    public static final String SPRING_CONFIG_FILE =
            "classpath:/spring-config.xml";
    private ModelConstants () {}

}
