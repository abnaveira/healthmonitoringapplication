package es.udc.tfg.healthmonitoringapplication.model.exceptions;

public class InvalidAuthenticationTokenException extends RuntimeException {
    /*Excepción cuando el token tiene algún error. p.ej: ha caducado o ha sido modificadol.*/
    public InvalidAuthenticationTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
