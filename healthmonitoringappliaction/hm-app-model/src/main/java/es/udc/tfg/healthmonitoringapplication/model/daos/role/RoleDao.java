package es.udc.tfg.healthmonitoringapplication.model.daos.role;

import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDao;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

public interface RoleDao extends GenericDao<Role,Long> {
    /*Buscar un rol por su nombre*/
    Role findByRoleName(String roleName) throws InstanceNotFoundException;
}
