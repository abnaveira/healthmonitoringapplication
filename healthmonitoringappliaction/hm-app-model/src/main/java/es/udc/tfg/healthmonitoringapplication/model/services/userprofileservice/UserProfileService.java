package es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.exceptions.AuthenticationException;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.AuthenticationTokenDetails;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

import java.util.List;
import java.util.Set;

public interface UserProfileService {

    /*Crea un nuevo usuario, lanzando una excepción si el usuario ya existe*/
    UserProfile createUserProfile(UserProfile userProfile) throws DuplicateInstanceException;

    /*Acualiza el perfil lanzando una excepción si no existe. IMPORTANTE. No cambiar la contraseña ya que se almacena con un hash,
    * para cambiar la contraseña ya existe el método changePassword*/
    UserProfile updateUserProfile(UserProfile userProfile) throws InstanceNotFoundException;

    /*Busca un usuario por su id*/
    UserProfile findById(Long id) throws InstanceNotFoundException;

    /*Busca un usuario por su username*/
    UserProfile findByUsername(String username) throws InstanceNotFoundException;

    /*Realiza el login, se le puede pasar la contraseña en claro o hasheada con el parámetro encrypted. Lanza una excepción en caso de error.*/
    UserProfile login(String username, String password, boolean encrypted) throws AuthenticationException;

    /*Genera un token para el usuario dado.*/
    String issueToken(String username, Set<Long> authorities);

    /*Recibe el token y lo desglosa para ver su información*/
    AuthenticationTokenDetails parseToken(String token);

    /*Devuelve los ids de los roles de un usuario. No el rol en si*/
    Set<Long> getRolesIdFromUserId(Long usrId) throws InstanceNotFoundException;

    /*Cambia la contraseña de un usuario.*/
    void changePassword(Long usrId, String oldPassword, String newPassword) throws InstanceNotFoundException;

    /*Devuelve los usuarios que han compartido su usuario con el perfil dado*/
    List<UserProfile> getUserProfilesSharedWithMe(Long usrId,int startIndex,int count) throws InstanceNotFoundException;

    /*Devuelve los usuarios con los que el perfil dado ha compartido su perfil*/
    List<UserProfile> getUserProfilesISharedWith(Long usrId,int startIndex,int count) throws InstanceNotFoundException;

    /*Devuelve los resultados de la búsqueda por keywords*/
    List<UserProfile> getUsersByKeyword(String keyword,int startIndex,int count);

    /*Devuelve el número de resultados de la búsqueda por keywords*/
    int getNumberOfUsersByKeyword(String keyword);

    /*Compartir el usuario (usrId) con el usuario (shareWith) */
    void shareProfileWith(Long usrId,Long shareWith) throws InstanceNotFoundException;

    /*Cancelar la comparticion del usuario (usrId) con el usuario (shareWith)*/
    void cancelShareProfileWith(Long usrId,Long shareWith) throws InstanceNotFoundException;

    /*Devuelve el número de usuarios que han compartido su perfil con el usuario dado. (Se suma 1 porque también se incluye el propio usuario)*/
    int getNumberOfUsersSharedWithMe(Long usrId);

    /*Devuelve el number de usuarios  con los que se ha compartido el perfil*/
    int getNumberOfUsersISharedWith(Long usrId);
}
