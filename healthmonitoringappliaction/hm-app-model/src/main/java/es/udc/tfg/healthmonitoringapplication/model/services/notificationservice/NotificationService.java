package es.udc.tfg.healthmonitoringapplication.model.services.notificationservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.notification.Notification;
import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

import java.util.Calendar;
import java.util.List;

public interface NotificationService {

    /*Añade una notificacion a un usuario*/
    void addNewNotificationByName(String notificationName,String notificationValues, long usrId, Calendar date) throws InstanceNotFoundException;

    /*Recupera las notificaciones sin leer del usuario. markAsViewed indica que si tras recuperar esas notificaciones se quieren marcar como leídas.*/
    List<UserNotification> getUnreadUserNotifications(long usrId,boolean markAsViewed);

    /*Busca las notificaciones en un día. Con paginación.*/
    List<UserNotification> findUserNotifications(long usrId, Calendar date,int startIndex, int count);

    /*Devuelve el número de notificaciones el día dado.*/
    int getNumberOfNotifications(long usrId, Calendar date);
}
