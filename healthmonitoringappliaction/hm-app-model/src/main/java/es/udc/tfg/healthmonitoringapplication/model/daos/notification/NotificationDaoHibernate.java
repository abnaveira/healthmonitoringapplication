package es.udc.tfg.healthmonitoringapplication.model.daos.notification;

import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDaoHibernate;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import org.springframework.stereotype.Repository;

@Repository("notificationDao")
public class NotificationDaoHibernate extends GenericDaoHibernate<Notification,Long> implements NotificationDao{

    @Override
    public Notification findNotificationByName(String name) throws InstanceNotFoundException {
        Notification notification =(Notification) getSession().createQuery("SELECT r FROM Notification r WHERE notificationName = :notificationName")
                .setParameter("notificationName",name).uniqueResult();
        if(notification==null){
            throw new InstanceNotFoundException(name,Notification.class.getName());
        }
        return notification;
    }
}
