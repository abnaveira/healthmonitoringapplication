package es.udc.tfg.healthmonitoringapplication.model.daos.userprofile;

import es.udc.tfg.healthmonitoringapplication.model.exceptions.AuthenticationException;
import es.udc.tfg.healthmonitoringapplication.modelutil.dao.GenericDao;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

import java.util.List;

public interface UserProfileDao extends GenericDao<UserProfile,Long> {

    /*Busca un usuario por su usario, si no lo encuentra lanza un InstanceNotFoundException*/
    UserProfile findUserByUsername(String username) throws  InstanceNotFoundException;

    /*Devuelve la lista de usuarios que han compartido su perfil con el usuario dado. Con paginación*/
    List<UserProfile> getUserProfilesSharedWithMe(Long usrId,int startIndex,int count) throws InstanceNotFoundException;

    /*Devuelve la lista de usuarios con los que el usuario dado ha compartido su perfil. Con paginación*/
    List<UserProfile> getUserProfilesISharedWith(Long usrId, int startIndex,int count) throws InstanceNotFoundException;

    /*Que el usuario usrId comparta su perfil con shareWith*/
    void shareProfileWith(Long usrId,Long shareWith) throws InstanceNotFoundException;

    /*Cancelar la compartición del perfil usrId al perfil shareWith*/
    void cancelShareProfileWith(Long usrId,Long shareWith) throws InstanceNotFoundException;

    /*Busca usuarios por keywords, con paginacion*/
    List<UserProfile> findUsersByKeyword(String keyword,int startIndex,int count);

    /*Devuelve la cantidad de usuarios en búqueda por keywords*/
    int getNumberOfUsersByKeyword(String keyword);

    /*Devuelve la cantidad de usuarios compartidos con el usuario dado. Se incluye el propio usuario*/
    int getNumberOfUsersSharedWithMe(Long usrId);

    /*Devuelve la cantidad de usuarios con los que se está compartiendo el perfil*/
    int getNumberOfUsersISharedWith(Long usrId);
}
