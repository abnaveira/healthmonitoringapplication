package es.udc.tfg.healthmonitoringapplication.model.daos.measurement;

public enum MeasurementTimeRange {
    /*Los filtros usados para filtrar la busqueda de las medidas,
    * HOUR agrupa medidas cada  5 min, DAY cada hora, WEEK cada día, MONTH cada día y YEAR cada mes.*/
    HOUR,DAY,WEEK,MONTH,YEAR
}
