package es.udc.tfg.healthmonitoringapplication.modelutil.dao;

import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

import java.io.Serializable;

public interface GenericDao <E, PK extends Serializable> {

    /*DAO Generico que heredarán el resto de DAOS y que implementa las operaciones básicas de crear/actualizar, buscar y eliminar*/
    void save (E entity);

    E find(PK id) throws InstanceNotFoundException;

    void remove(PK id) throws InstanceNotFoundException;
}
