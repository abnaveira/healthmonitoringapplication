package es.udc.tfg.healthmonitoringapplication.model.daos.user_notification;

import es.udc.tfg.healthmonitoringapplication.model.daos.notification.Notification;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;

import javax.persistence.*;
import java.util.Calendar;

/*Esta entidad relaciona usuarios con notificaciones. Es una relación n a n*/
@Entity
@Table(name = "User_Notification")
public class UserNotification {
    private Long userNotificationId;/*Id de la notificación; autogenerado.*/
    /*notificationValues contiene los valores que tiene el cuerpo de la notificación,
    por ejemplo: la notificación de pulsaciones altas indica que valor de pulsaciones le llevó a
    esa conclusión, y eso es un valor que varía. Si la notificación tiene varios valores se separan por un espacio, luego
    en el cliente se insertan en el cuerpo de la notificación. Como apunte decir que el título y el cuerpo de la notificación
    se almacena en el cliente ya que simplifica el código a la hora de manejar las traducciones.*/
    private String notificationValues;
    /*Perfil del usuario*/
    private UserProfile userProfile;
    /*Notificación asociada*/
    private Notification notification;
    /*Fecha de generación de esa notificación al usuario.*/
    private Calendar date;
    /*La notificación a sido enviada al usuario o no*/
    private Boolean viewed;

    public UserNotification() {
    }

    public UserNotification(UserProfile userProfile,String notificationValues, Notification notification, Calendar date, Boolean viewed) {
        this.notificationValues=notificationValues;
        this.userProfile = userProfile;
        this.notification = notification;
        this.date = date;
        this.viewed=viewed;
    }

    @Column(name = "userNotificationId")
    @SequenceGenerator(
            name="UserNotificationIdGenerator",
            sequenceName = "UserNotificationSequence"
    )
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,
            generator = "UserNotificationIdGenerator")
    public Long getUserNotificationId() {
        return userNotificationId;
    }

    public void setUserNotificationId(Long userNotificationId) {
        this.userNotificationId = userNotificationId;
    }

    @ManyToOne
    @JoinColumn(name = "usrId")
    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
    @ManyToOne
    @JoinColumn(name = "notificationId")
    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    @Column(name = "viewed")
    public Boolean getViewed() {
        return viewed;
    }

    public void setViewed(Boolean viewed) {
        this.viewed = viewed;
    }

    @Column(name="notificationValues")
    public String getNotificationValues() {
        return notificationValues;
    }

    public void setNotificationValues(String notificationValues) {
        this.notificationValues = notificationValues;
    }
}
