package es.udc.tfg.healthmonitoringapplication.model.daos.measurement;

/*
* Esta es una clase  para mostrar estadísticas de las medidas aplicando un filtro
* del tipo MeasurementTimeRange. Tiene información sobre la media, desviacion, mínimo valor y máximo valor de pulsaciones.
* */

public class MeasurementsStats {
    private Float meanHr;
    private Float deviationHr;
    private Float peakHr;
    private Float lowHr;
    private boolean isEmpty;

    public MeasurementsStats(Float meanHr, Float deviationHr, Float peakHr, Float lowHr) {
        this.meanHr = meanHr;
        this.deviationHr = deviationHr;
        this.peakHr = peakHr;
        this.lowHr = lowHr;
        isEmpty= (meanHr==null || deviationHr==null || peakHr==null || lowHr==null);
    }

    public Float getMeanHr() {
        return meanHr;
    }

    public void setMeanHr(Float meanHr) {
        this.meanHr = meanHr;
        isEmpty= this.meanHr==null || this.deviationHr==null || this.peakHr==null || this.lowHr==null;

    }

    public Float getDeviationHr() {
        return deviationHr;
    }

    public void setDeviationHr(Float deviationHr) {
        this.deviationHr = deviationHr;
        isEmpty= this.meanHr==null || this.deviationHr==null || this.peakHr==null || this.lowHr==null;

    }

    public Float getPeakHr() {
        return peakHr;
    }

    public void setPeakHr(Float peakHr) {
        this.peakHr = peakHr;
        isEmpty= this.meanHr==null || this.deviationHr==null || this.peakHr==null || this.lowHr==null;

    }

    public Float getLowHr() {
        return lowHr;
    }

    public void setLowHr(Float lowHr) {
        this.lowHr = lowHr;
        isEmpty= this.meanHr==null || this.deviationHr==null || this.peakHr==null || this.lowHr==null;

    }

    public boolean isEmpty() {
        return isEmpty;
    }
}
