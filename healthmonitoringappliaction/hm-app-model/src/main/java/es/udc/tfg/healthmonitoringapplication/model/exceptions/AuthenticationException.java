package es.udc.tfg.healthmonitoringapplication.model.exceptions;

public class AuthenticationException extends RuntimeException {

    /*Excepción cuando ha habido un error en la autenticación*/
    public AuthenticationException(String message) {
        super(message);
    }
    /*public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }*/
}
