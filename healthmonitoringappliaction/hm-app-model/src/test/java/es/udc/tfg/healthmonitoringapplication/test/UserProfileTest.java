package es.udc.tfg.healthmonitoringapplication.test;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.healthstatsservice.HealthStatsService;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.util.AuthenticationTokenDetails;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.test.util.TestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;
import static es.udc.tfg.healthmonitoringapplication.test.util.ModelTestConstants.SPRING_CONFIG_TEST_FILE;

@RunWith(SpringJUnit4ClassRunner.class)
/*El segundo fichero de configuración sobreescribe el datasource definido por el primero*/
@ContextConfiguration(locations ={SPRING_CONFIG_FILE,SPRING_CONFIG_TEST_FILE})
@Transactional
public class UserProfileTest {

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private HealthStatsService healthStatsService;

    private TestUtil testUtil;

    @Before
    public void initObjects() {
        testUtil = new TestUtil(roleService,userProfileService);
    }
    @Test
    public void testCreateAndFindUser() throws InstanceNotFoundException, DuplicateInstanceException {
        /*Se crea un usuario*/
        UserProfile userProfile=testUtil.createUserProfile("user1");
        /*Se busca el usuario por id*/
        UserProfile userProfile1 = userProfileService.findById(userProfile.getUsrId());
        /*Se busca el usuario por username*/
        UserProfile userProfile2 = userProfileService.findByUsername(userProfile.getUsername());
        /*Se comprueba que los resultados son correctos.*/
        assertEquals(userProfile,userProfile1);
        assertEquals(userProfile,userProfile2);
    }

    @Test
    public void testUpdateUserProfile() throws InstanceNotFoundException, DuplicateInstanceException {
        /*Se crea un usuario*/
        UserProfile userProfile =testUtil.createUserProfile("user1");
        /*Se le cambia el nombre.*/
        userProfile.setName("Adrian");
        /*Se guarda el cambio*/
        UserProfile userProfile1 = userProfileService.updateUserProfile(userProfile);
        /*se comprueba que se ha cambiado*/
        Assert.assertEquals(userProfile1.getName(),"Adrian");
    }

    @Test
    public void testLoginAndChangePassword() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea un usuario*/
        UserProfile userProfile =testUtil.createUserProfile("user1");
        /*Se hace login con la contraseña encriptada y sin encriptar. La encriptada es la que está en UserProfile*/
        UserProfile userProfile1 = userProfileService.login(userProfile.getUsername(), userProfile.getPassword(), true);
        UserProfile userProfile2 = userProfileService.login(userProfile.getUsername(),TestUtil.password,false);
        /*Se comprueba que que el usuario devuelto es el correcto*/
        assertEquals(userProfile,userProfile1);
        assertEquals(userProfile,userProfile2);
        /*Se cambia la contraseña*/
        userProfileService.changePassword(userProfile.getUsrId(),TestUtil.password,TestUtil.password2);
        UserProfile userProfile3 = userProfileService.findByUsername(userProfile.getUsername());
        /*Se comprueba que que se ha cambiado correctamente.*/
        assertEquals(userProfile,userProfile3);
    }

    @Test
    public void testToken() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea el usuario*/
        UserProfile userProfile =testUtil.createUserProfile("user1");
        /*Se genera un token*/
        String token = userProfileService.issueToken(userProfile.getUsername(),userProfileService.getRolesIdFromUserId(userProfile.getUsrId()));
        /*Se parsea el token*/
        AuthenticationTokenDetails authenticationTokenDetails = userProfileService.parseToken(token);
        /*Se comprueba que el token contiene los valores del usuario*/
        assertEquals(authenticationTokenDetails.getUsername(),userProfile.getUsername());
    }

    @Test
    public void testGetRolesFromUserId() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea un usuario*/
        UserProfile userProfile =testUtil.createUserProfile("user1");
        /*Se comprueba que el se devuelve el rol USER del usuario.*/
        Set<Long> roles = userProfileService.getRolesIdFromUserId(userProfile.getUsrId());
        Role role = roleService.findByRoleName("USER");
        assertTrue(roles.contains(role.getRoleId()));
        assertTrue(roles.size()==1);
    }

    @Test
    public void testUsersShared() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crean dos usuarios.*/
        UserProfile userProfile1 =testUtil.createUserProfile("user1");
        UserProfile userProfile2 =testUtil.createUserProfile("user2");
        /*el usuario2 comparte su perfil con el usuario 1*/
        userProfileService.shareProfileWith(userProfile2.getUsrId(),userProfile1.getUsrId());

        /*Se recupera la lista de usuarios compartidas con el usuario1*/
        List<UserProfile> userProfilesSharedWithMe = userProfileService.getUserProfilesSharedWithMe(userProfile1.getUsrId(), 0, 10);
        /*Se recupera la lista de usuarios con los que el usuario2 comparte su perfil*/
        List<UserProfile> userProfilesISharedWith = userProfileService.getUserProfilesISharedWith(userProfile2.getUsrId(), 0, 10);
        /*Se comprueba que los resultados sean correctos*/
        //La posicion 0 siempre sera uno mismo.
        assertEquals(userProfilesSharedWithMe.get(1).getUsrId(),userProfile2.getUsrId());
        assertEquals(userProfilesISharedWith.get(0).getUsrId(),userProfile1.getUsrId());

        /*Se busca el número de resultados de los casos anteriores*/
        int numberOfUsersSharedWithMe = userProfileService.getNumberOfUsersSharedWithMe(userProfile1.getUsrId());
        int numberOfUsersISharedWith = userProfileService.getNumberOfUsersISharedWith(userProfile2.getUsrId());

        /*Se comprueban los resultados*/
        //Solo habra dos si es compartiendo conmigo ya que se incluye el propio usuario
        assertEquals(numberOfUsersSharedWithMe,2);
        assertEquals(numberOfUsersISharedWith,1);

        /*Se cancela la compartición del usuario2 con el usuario1*/
        userProfileService.cancelShareProfileWith(userProfile2.getUsrId(),userProfile1.getUsrId());

        /*Se vuelve a comprobar que se haya realizado la operación.*/
        int numberOfUsersSharedWithMe2 = userProfileService.getNumberOfUsersSharedWithMe(userProfile1.getUsrId());
        int numberOfUsersISharedWith2 = userProfileService.getNumberOfUsersISharedWith(userProfile2.getUsrId());

        //Solo habra dos si es compartiendo conmigo
        assertEquals(numberOfUsersSharedWithMe2,1);
        assertEquals(numberOfUsersISharedWith2,0);
    }

    @Test
    public void testUserByKeywords() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se insertan dos usuarios.*/
        UserProfile userProfile1 =testUtil.createUserProfile("user1");
        UserProfile userProfile2 =testUtil.createUserProfile("user2");

        /*Se realiza una búsqueda por keywords*/
        List<UserProfile> users = userProfileService.getUsersByKeyword("user", 0, 10);
        /*Se busca el número de resultados de la búsqueda por keywords.*/
        int numberOfUsersByKeyword = userProfileService.getNumberOfUsersByKeyword("user");
        /*Se comprueba que son los resultados esperados*/
        assertEquals(users.get(0),userProfile1);
        assertEquals(numberOfUsersByKeyword,2);
    }
}
