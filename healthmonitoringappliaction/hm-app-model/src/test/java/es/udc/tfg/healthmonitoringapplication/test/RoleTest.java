package es.udc.tfg.healthmonitoringapplication.test;


import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.notificationservice.NotificationService;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.test.util.TestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;
import static es.udc.tfg.healthmonitoringapplication.test.util.ModelTestConstants.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations ={SPRING_CONFIG_FILE,SPRING_CONFIG_TEST_FILE})
@Transactional
public class RoleTest {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserProfileService userProfileService;

    private TestUtil testUtil;

    @Before
    public void initObjects() {
        testUtil = new TestUtil(roleService,userProfileService);
    }

    @Test
    public void testFindRoleByName() throws InstanceNotFoundException {
        Role user = roleService.findByRoleName("USER");
        assertTrue(user.getRoleName().equals("USER"));
    }

    @Test
    public void testAddRole() throws InstanceNotFoundException, DuplicateInstanceException {
        /*Se crea un usuario*/
        UserProfile userProfile=testUtil.createUserProfile("user1");
        /*Se le añade el rol ADMIN al usuario*/
        roleService.addRoleToUser(userProfile.getUsrId(),"ADMIN");
        /*Se busca el usuario*/
        UserProfile userProfile1 = userProfileService.findById(userProfile.getUsrId());
        /*Se busca el rol*/
        Role role = roleService.findByRoleName("ADMIN");
        /*Se comprueba que el usuario contiene ese rol*/
        Assert.assertTrue(userProfile1.getRoles().contains(role));
    }

    @Test
    public void testDeleteRole() throws InstanceNotFoundException, DuplicateInstanceException {
        /*Se crea un usuario. (por defecto tiene el rol USER)*/
        UserProfile userProfile =testUtil.createUserProfile("user1");
        /*Se borra el rol USER*/
        roleService.deleteRoleFromUser(userProfile.getUsrId(),"USER");
        /*Se recupera el usuario*/
        UserProfile userProfile1 = userProfileService.findById(userProfile.getUsrId());
        /*Se busca el rol*/
        Role role = roleService.findByRoleName("USER");
        /*Se comprueba que el usuario ya no contiene el rol*/
        Assert.assertTrue(!userProfile1.getRoles().contains(role));
    }
}
