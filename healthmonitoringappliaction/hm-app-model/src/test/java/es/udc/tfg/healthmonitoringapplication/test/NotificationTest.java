package es.udc.tfg.healthmonitoringapplication.test;


import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.healthstatsservice.HealthStatsService;
import es.udc.tfg.healthmonitoringapplication.model.services.notificationservice.NotificationService;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.test.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import java.util.Calendar;
import java.util.List;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;
import static es.udc.tfg.healthmonitoringapplication.test.util.ModelTestConstants.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations ={SPRING_CONFIG_FILE,SPRING_CONFIG_TEST_FILE})
@Transactional
public class NotificationTest {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private NotificationService notificationService;

    private TestUtil testUtil;

    @Before
    public void initObjects() {
        testUtil = new TestUtil(roleService,userProfileService);
    }

    @Test
    public void testAll() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea un usuario*/
        UserProfile userProfile=testUtil.createUserProfile("user1");
        /*Se añade una nueva notificación al usuario*/
        notificationService.addNewNotificationByName("NO_MOVEMENT","",userProfile.getUsrId(), Calendar.getInstance());
        /*Se recuperan las notificaciones no leídas del usuario*/
        List<UserNotification> unreadUserNotifications = notificationService.getUnreadUserNotifications(userProfile.getUsrId(), true);
        /*Se comprueba que solo está la notificación que se añadió previamente.*/
        assertTrue(unreadUserNotifications.size()==1);
        assertTrue(unreadUserNotifications.get(0).getNotification().getNotificationName().equals("NO_MOVEMENT"));
        /*Se buscan las notificaciones del dia actual*/
        List<UserNotification> userNotifications = notificationService.findUserNotifications(userProfile.getUsrId(), Calendar.getInstance(), 0, 10);
        /*se comprueba que coincide con la notificación añadida.*/
        assertTrue(userNotifications.size()==1);
        assertTrue(userNotifications.get(0).getNotification().getNotificationName().equals("NO_MOVEMENT"));
        /*Devuelve el número de notificaciones no leídas y se comprueba que es el resultado esperado.*/
        int numberOfNotifications = notificationService.getNumberOfNotifications(userProfile.getUsrId(), Calendar.getInstance());
        assertTrue(numberOfNotifications==1);
    }
}
