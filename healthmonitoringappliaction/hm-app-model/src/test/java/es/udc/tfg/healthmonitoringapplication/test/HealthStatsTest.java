package es.udc.tfg.healthmonitoringapplication.test;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.healthstatsservice.HealthStatsService;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.test.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;
import static es.udc.tfg.healthmonitoringapplication.test.util.ModelTestConstants.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
/*El segundo fichero de configuración sobreescribe el datasource definido por el primero*/
@ContextConfiguration(locations ={SPRING_CONFIG_FILE,SPRING_CONFIG_TEST_FILE})
@Transactional
public class HealthStatsTest {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private HealthStatsService healthStatsService;

    private TestUtil testUtil;

    /*Se ejecuta antes de los tests*/
    @Before
    public void initObjects() {
        testUtil = new TestUtil(roleService,userProfileService);
    }

    @Test
    public void testCreateOrUpdateHealthStats() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea el usuario*/
        HealthStats healthStats = new HealthStats(18,180,(float) 76, Gender.MALE, LifeStyle.MEDIUM_EXERCISE);
        UserProfile user1 = testUtil.createUserProfile("user1", healthStats);
        /*Se cambia el estilo de vida */
        healthStats.setLifeStyle(LifeStyle.HIGH_EXERCISE);
        /*Se actualizan las estadísticas*/
        HealthStats healthStats1 = healthStatsService.createOrUpdateHealthStats(healthStats);
        /*Se comprueba que se ha actualizado*/
        assertTrue(healthStats.getLifeStyle().equals(healthStats1.getLifeStyle()));
    }
}
