package es.udc.tfg.healthmonitoringapplication.test.util;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

public class TestUtil {
    public static String password="querty";
    public static String password2="asdfgh";

    RoleService roleService;
    UserProfileService userProfileService;

    public TestUtil(RoleService roleService, UserProfileService userProfileService) {
        this.roleService = roleService;
        this.userProfileService = userProfileService;
    }

    /*crea un usuario con el username dado. El resto de valores son constantes*/
    public UserProfile createUserProfile(String username) throws InstanceNotFoundException, DuplicateInstanceException {
        String name = username.substring(0, 1).toUpperCase() + username.substring(1);
        Role role = roleService.findByRoleName("USER");
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(role);
        HealthStats healthStats = new HealthStats(18,180,(float) 76, Gender.MALE, LifeStyle.MEDIUM_EXERCISE);
        UserProfile u = new UserProfile(name,username,password,roleSet);
        u.setHealthStats(healthStats);
        healthStats.setUserProfile(u);
        UserProfile userProfile = userProfileService.createUserProfile(u);
        return userProfile;
    }

    /*Igual pero también se pueden establecer las estadísticas*/
    public UserProfile createUserProfile(String username,HealthStats healthStats) throws InstanceNotFoundException, DuplicateInstanceException {
        String name = username.substring(0, 1).toUpperCase() + username.substring(1);
        Role role = roleService.findByRoleName("USER");
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(role);
        UserProfile u = new UserProfile(username,name,"qwerty",roleSet);
        u.setHealthStats(healthStats);
        healthStats.setUserProfile(u);
        UserProfile userProfile = userProfileService.createUserProfile(u);
        return userProfile;
    }


    /*Crea una medida aleatoria para un usuario dado.*/
    public Measurement createRandomMeasurement(UserProfile userProfile, Calendar date){
        Integer hr = (int) (Math.random() * ((200) + 1));
        Long steps = (long) (100 + (int) (Math.random() * ((4000 - 100) + 1)));
        Long calories = (long) (100 + (int) (Math.random() * ((4000 - 100) + 1)));
        Long distance = (long) (100 + (int) (Math.random() * ((4000 - 100) + 1)));
        Measurement m = new Measurement("A1:B2:C3:D4:E5",hr,steps,calories,distance,date);
        m.setUserProfile(userProfile);
        return m;
    }
}
