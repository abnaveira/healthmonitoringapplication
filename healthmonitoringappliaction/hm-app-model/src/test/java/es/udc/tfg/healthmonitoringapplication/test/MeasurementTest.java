package es.udc.tfg.healthmonitoringapplication.test;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementTimeRange;
import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.MeasurementsStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.measurementservice.MeasurementService;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.test.util.TestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.*;

import static es.udc.tfg.healthmonitoringapplication.model.util.ModelConstants.SPRING_CONFIG_FILE;
import static es.udc.tfg.healthmonitoringapplication.test.util.ModelTestConstants.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations ={SPRING_CONFIG_FILE,SPRING_CONFIG_TEST_FILE})
@Transactional
public class MeasurementTest {

    @Autowired
    private MeasurementService measurementService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserProfileService userProfileService;

    private TestUtil testUtil;

    /*Comprueba que los valores de dos mediciones son los mismos*/
    private boolean compareMeasurementValues(Measurement m1,Measurement m2){
        return m1.getHeartRate().equals(m2.getHeartRate()) && m1.getSteps().equals(m2.getSteps()) && m1.getCalories().equals(m2.getCalories()) && m1.getDistance().equals(m2.getDistance());
    }

    /*Comprueba que los valores de los stats on los mismos*/
    private boolean compareMeasurementStats(MeasurementsStats m1,MeasurementsStats m2){
        return m1.getDeviationHr().equals(m2.getDeviationHr()) && m1.getLowHr().equals(m2.getLowHr()) && m1.getMeanHr().equals(m2.getMeanHr())&& m1.getPeakHr().equals(m2.getPeakHr());
    }

    @Before
    public void initObjects() {
        testUtil = new TestUtil(roleService,userProfileService);
    }

    @Test
    public void testCreateMeasurementAndFindMeasurement() throws InstanceNotFoundException, DuplicateInstanceException {
        /*Se crea un usuario y una medida*/
        Measurement measurement = measurementService.createMeasurement(testUtil.createRandomMeasurement(testUtil.createUserProfile("user1"),Calendar.getInstance()));
        /*Se recupera la medida y se compara*/
        Measurement measurement1 = measurementService.findMeasurement(measurement.getMeasurementId());
        assertEquals(measurement,measurement1);
    }

    @Test
    public void testGetMeasurementsInRangeById() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea un usuario y una medida.*/
        Measurement measurement = measurementService.createMeasurement(testUtil.createRandomMeasurement(testUtil.createUserProfile("user1"),Calendar.getInstance()));
        /*Se realizan busquedas con todos los filtros*/
        List<Measurement> measurements1= measurementService.getMeasurementsInRangeById(measurement.getUserProfile().getUsrId(), MeasurementTimeRange.HOUR, 0, 10);
        List<Measurement> measurements2= measurementService.getMeasurementsInRangeById(measurement.getUserProfile().getUsrId(), MeasurementTimeRange.DAY, 0, 10);
        List<Measurement> measurements3= measurementService.getMeasurementsInRangeById(measurement.getUserProfile().getUsrId(), MeasurementTimeRange.WEEK, 0, 10);
        List<Measurement> measurements4= measurementService.getMeasurementsInRangeById(measurement.getUserProfile().getUsrId(), MeasurementTimeRange.MONTH, 0, 10);
        List<Measurement> measurements5= measurementService.getMeasurementsInRangeById(measurement.getUserProfile().getUsrId(), MeasurementTimeRange.YEAR, 0, 10);

        /*Se comprueba que el único resultado de cada búsqueda es la medida originialmente isertada. (La fecha y el bandId no serán iguales por eso es usa compareMeasurementValues) */
        assertTrue(compareMeasurementValues(measurement,measurements1.get(0)));
        assertTrue(compareMeasurementValues(measurement,measurements2.get(0)));
        assertTrue(compareMeasurementValues(measurement,measurements3.get(0)));
        assertTrue(compareMeasurementValues(measurement,measurements4.get(0)));
        assertTrue(compareMeasurementValues(measurement,measurements5.get(0)));
    }

    @Test
    public void testGetMeasurementsInAllRanges() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea una medida y un usuario*/
        Measurement measurement = measurementService.createMeasurement(testUtil.createRandomMeasurement(testUtil.createUserProfile("user1"),Calendar.getInstance()));
        /*Básicamente se hace lo mismo que en testGetMeasurementsInRangeById*/
        List<List<Measurement>> measurementsInAllRangesById = measurementService.getMeasurementsInAllRangesById(measurement.getUserProfile().getUsrId());
        for(List<Measurement> m : measurementsInAllRangesById){
            assertTrue(compareMeasurementValues(measurement,m.get(0)));
        }
    }

    @Test
    public void testGetMeasurementsByUserIdByDate() throws DuplicateInstanceException, InstanceNotFoundException {
        Calendar calendar1 = Calendar.getInstance();
        /*Se crea un usuario.*/
        UserProfile userProfile=testUtil.createUserProfile("user1");
        /*Se inserta una medida en el instante actual*/
        Measurement measurement1 = measurementService.createMeasurement(testUtil.createRandomMeasurement(userProfile,calendar1));
        Calendar calendar2=Calendar.getInstance();
        calendar2.add(Calendar.DAY_OF_MONTH,-1);
        /*Se crea una medida el día anterior*/
        measurementService.createMeasurement(testUtil.createRandomMeasurement(userProfile,calendar2));
        Calendar searchCalendar=Calendar.getInstance();
        /*Se buscan medidas en el día actual*/
        List<Measurement> measurementsByUserIdByDate = measurementService.getMeasurementsByUserIdByDate(userProfile.getUsrId(), searchCalendar, 0, 10);
        /*Se compruea que solo hay un resultado y que es el insertado el día actual*/
        assertTrue(measurementsByUserIdByDate.size()==1);
        assertTrue(compareMeasurementValues(measurement1,measurementsByUserIdByDate.get(0)));
    }

    @Test
    public void testGetNumberOfMeasurements() throws DuplicateInstanceException, InstanceNotFoundException {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.DAY_OF_MONTH,4);
        /*Se crea un usuario*/
        UserProfile userProfile=testUtil.createUserProfile("user1");
        /*Se crea una medida el día actual*/
        measurementService.createMeasurement(testUtil.createRandomMeasurement(userProfile,calendar1));
        Calendar calendar2=Calendar.getInstance();
        calendar2.add(Calendar.DAY_OF_MONTH,3);
        /*Se crea una medida el día anterior.*/
        measurementService.createMeasurement(testUtil.createRandomMeasurement(userProfile,calendar2));
        Calendar searchCalendar=Calendar.getInstance();
        searchCalendar.set(Calendar.DAY_OF_MONTH,4);
        /*Se consiguen el número de resultados para la búsqueda con filtro y por día*/
        int numberOfMeasurements = measurementService.getNumberOfMeasurements(userProfile.getUsrId(), MeasurementTimeRange.MONTH);
        int numberOfMeasurementsByDate = measurementService.getNumberOfMeasurementsByDate(userProfile.getUsrId(), searchCalendar);
        /*Se comprueba que son los valores esperados.*/
        assertTrue(numberOfMeasurements==2);
        assertTrue(numberOfMeasurementsByDate==1);
    }

    @Test
    public void testGetMeasurementsStats() throws DuplicateInstanceException, InstanceNotFoundException {
        Calendar calendar1 = Calendar.getInstance();
        /*Se crea un usuario y se inserta.*/
        UserProfile userProfile=testUtil.createUserProfile("user1");
        Measurement measurement1 = measurementService.createMeasurement(testUtil.createRandomMeasurement(userProfile,calendar1));
        /*Se crea un MeasurementsStats manualmente con los valores esperados*/
        MeasurementsStats measurementsStats = new MeasurementsStats((float) measurement1.getHeartRate(),(float)0,(float) measurement1.getHeartRate(),(float) measurement1.getHeartRate());
        /*Se busca el MeasurementsStats*/
        MeasurementsStats measurementsStats1 = measurementService.getMeasurementsStats(userProfile.getUsrId(), MeasurementTimeRange.HOUR);
        /*Se comprueba que son los valores esperados.*/
        assertTrue(compareMeasurementStats(measurementsStats,measurementsStats1));
    }


}
