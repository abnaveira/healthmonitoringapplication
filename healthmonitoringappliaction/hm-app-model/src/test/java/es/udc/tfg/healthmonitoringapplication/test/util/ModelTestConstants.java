package es.udc.tfg.healthmonitoringapplication.test.util;

public class ModelTestConstants {

    public static final String SPRING_CONFIG_TEST_FILE =
            "classpath:/spring-config-test.xml";

    private ModelTestConstants () {}
}
