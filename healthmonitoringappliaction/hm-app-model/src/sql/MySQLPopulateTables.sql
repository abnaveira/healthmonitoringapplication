INSERT INTO Role (roleName) VALUES ('ADMIN');

INSERT INTO Role (roleName) VALUES ('USER');

INSERT INTO Notification(notificationName,alertLevel) VALUES ('NO_MOVEMENT',1);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('HIGH_CARDIAC_FREQ',2);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('LOW_CARDIAC_FREQ',2);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('VERY_HIGH_CARDIAC_FREQ',3);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('VERY_LOW_CARDIAC_FREQ',3);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('IRREGULAR_CARDIAC_FREQ',3);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('CONTINUOUS_HIGH_CARDIAC_FREQ',3);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('CONTINUOUS_LOW_CARDIAC_FREQ',3);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('RECOMMENDATION_OVERWEIGHT',0);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('RECOMMENDATION_OBESITY',2);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('RECOMMENDATION_UNDERWEIGHT',2);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('RECOMMENDATION_NORMALWEIGHT',0);

INSERT INTO Notification(notificationName,alertLevel) VALUES ('DAILY_HR_RESUME',0);


