-- noinspection SqlNoDataSourceInspectionForFile

DROP TABLE Measurement;
DROP TABLE User_Role;
DROP TABLE Role;
DROP TABLE User_Notification;
DROP TABLE Notification;
DROP TABLE HealthStats;
DROP TABLE UserShare;
DROP TABLE UserProfile;




CREATE TABLE UserProfile (usrId BIGINT NOT NULL AUTO_INCREMENT,name VARCHAR (20) NOT NULL, username VARCHAR (30) NOT NULL,
 password CHAR(60) NOT NULL, CONSTRAINT UserProfilePK PRIMARY KEY  (usrId), CONSTRAINT UniqueUsername UNIQUE (username)) ENGINE = InnoDB;

CREATE TABLE UserShare(doctor BIGINT NOT NULL,pacient BIGINT NOT NULL,
CONSTRAINT PK_doctor_pacient PRIMARY KEY (doctor,pacient),
CONSTRAINT FK_UserShare_doctor FOREIGN KEY (doctor) REFERENCES UserProfile(usrId),
CONSTRAINT FK_UserShare_pacient FOREIGN KEY (pacient) REFERENCES UserProfile(usrId)) ENGINE = InnoDB;

 CREATE TABLE Role (roleId BIGINT NOT NULL AUTO_INCREMENT, roleName VARCHAR (20) NOT NULL,
  CONSTRAINT PK_roleId PRIMARY KEY (roleId),
  CONSTRAINT UniqueRoleName UNIQUE (roleName)
) ENGINE=InnoDB;

CREATE TABLE User_Role (roleId BIGINT NOT NULL, usrId BIGINT NOT NULL,
 CONSTRAINT PK_roleId_usrId PRIMARY KEY (roleId,usrId),
 CONSTRAINT FK_User_Role_usrId FOREIGN KEY (usrId) REFERENCES UserProfile (usrId) ON DELETE CASCADE ,
 CONSTRAINT FK_User_role_roleId FOREIGN KEY (roleId) REFERENCES Role (roleId) ON DELETE CASCADE
  ) ENGINE = InnoDB;

CREATE TABLE Measurement (measurementId BIGINT NOT NULL AUTO_INCREMENT,bandId VARCHAR (20) NOT NULL, heartRate INTEGER NOT NULL,
steps BIGINT NOT NULL, calories BIGINT NOT NULL, distance BIGINT NOT NULL,date DATETIME NOT NULL,usrId BIGINT NOT NULL,
 CONSTRAINT MeasurementsPK PRIMARY KEY (measurementId), CONSTRAINT FK_Measurement_usrId FOREIGN KEY (usrId) REFERENCES UserProfile (usrId) ON DELETE CASCADE ) ENGINE = InnoDB;

CREATE TABLE HealthStats(usrId BIGINT NOT NULL,age INTEGER NOT NULL, height INTEGER NOT NULL, weight FLOAT NOT NULL,gender INTEGER NOT NULL,lifeStyle INTEGER NOT NULL,
 CONSTRAINT PK_HealthStats_usrId PRIMARY KEY (usrId),
 CONSTRAINT FK_HealthStats_usrId FOREIGN KEY (usrId) REFERENCES UserProfile (usrid) ON DELETE CASCADE) ENGINE = InnoDB;

CREATE TABLE Notification(notificationId BIGINT NOT NULL AUTO_INCREMENT,notificationName VARCHAR (30) NOT NULL,alertLevel INTEGER NOT NULL,
  CONSTRAINT PK_notificationId PRIMARY KEY (notificationId),
  CONSTRAINT UniqueNotificationName UNIQUE (notificationName))ENGINE = InnoDB;

CREATE TABLE User_Notification(userNotificationId BIGINT NOT NULL AUTO_INCREMENT,usrId BIGINT NOT NULL, notificationId BIGINT NOT NULL,notificationValues VARCHAR (60) NOT NULL, date DATETIME NOT NULL, viewed BOOLEAN NOT NULL,
  CONSTRAINT PK_usrId_notificationId PRIMARY KEY (userNotificationId),
  CONSTRAINT FK_User_Notification_usrId FOREIGN KEY (usrId) REFERENCES UserProfile (usrId) ,
  CONSTRAINT FK_User_Notification_notificationId FOREIGN KEY (notificationId) REFERENCES Notification (notificationId)) ENGINE =InnoDB;



