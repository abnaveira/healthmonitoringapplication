package es.udc.tfg.healthmonitoringapplication.test;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.HealthStats;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;
import es.udc.tfg.healthmonitoringapplication.model.daos.role.Role;
import es.udc.tfg.healthmonitoringapplication.model.daos.user_notification.UserNotification;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.notificationservice.NotificationService;
import es.udc.tfg.healthmonitoringapplication.model.services.roleservice.RoleService;
import es.udc.tfg.healthmonitoringapplication.model.services.userprofileservice.UserProfileService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.DuplicateInstanceException;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice.DrlMeasurement;
import es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice.DrlPerson;
import es.udc.tfg.healthmonitoringapplication.rulesystem.util.DrlConversor;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.drools.core.time.SessionPseudoClock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.api.runtime.conf.TimedRuleExecutionOption;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations ={"classpath:spring-config.xml","classpath*:spring-config-test.xml"})
@Transactional
public class MonitoringServiceTest {

    KieSession kieSession;

    @Autowired
    UserProfileService userProfileService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    RoleService roleService;

    /*método auxiliar para crear un usuario por defecto.*/
    private UserProfile createUserProfile(String username, HealthStats healthStats) throws InstanceNotFoundException, DuplicateInstanceException {
        String name = username.substring(0, 1).toUpperCase() + username.substring(1);
        Role role = roleService.findByRoleName("USER");
        Set<Role> roleSet = new HashSet<Role>();
        roleSet.add(role);
        UserProfile u = new UserProfile(username,name,"qwerty",roleSet);
        u.setHealthStats(healthStats);
        healthStats.setUserProfile(u);
        UserProfile userProfile = userProfileService.createUserProfile(u);
        return userProfile;
    }

    /*Comprueba que una lista de roles tiene el rol con el nombre notificationName*/
    private boolean containsNotification(String notificationName, List<UserNotification> userNotifications){
        for (UserNotification userNotification:userNotifications){
            if(userNotification.getNotification().getNotificationName().equals(notificationName)){
                return true;
            }
        }
        return false;
    }

    /*Se ejecuta antes de los tests*/
    @Before
    public void init(){
        /*Se crea el builder*/
        KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        /*Se especifica donde esta el fichero de reglas*/
        knowledgeBuilder.add(ResourceFactory.newClassPathResource("rules.drl"), ResourceType.DRL);

        /*Se configura la base de conocimento*/
        KieBaseConfiguration kieBaseConfiguration = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
        kieBaseConfiguration.setOption(EventProcessingOption.STREAM);
        KieBase kieBase = knowledgeBuilder.newKieBase();

        /*Se configura la sesión*/
        KieSessionConfiguration sessionConfiguration = KnowledgeBaseFactory.newKnowledgeSessionConfiguration();
        sessionConfiguration.setOption( TimedRuleExecutionOption.YES );
        /*El reloj será pseudo para poder avanzar y retroceder el tiempo*/
        sessionConfiguration.setOption(ClockTypeOption.get("pseudo"));
        kieSession = kieBase.newKieSession(sessionConfiguration,null);

        /*Se establecen las variables en los ficheros de reglas*/
        kieSession.setGlobal("notificationService",notificationService);
        kieSession.setGlobal("steps_threshold",50);
    }

    @Test
    public void testBodyStyle() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea un usuario con un peso normal y se inserta en la sesión*/
        UserProfile user1 = createUserProfile("user1", new HealthStats(21, 180, 75f, Gender.MALE, LifeStyle.LIGHT_EXERCISE));
        DrlPerson drlPerson1 = DrlConversor.userProfileToDrlPerson(user1);
        FactHandle factHandle = kieSession.insert(drlPerson1);

        kieSession.fireAllRules();
        /*Se compruebsa que se ha creado el hecho Normalweight con una query*/
        QueryResults results = kieSession.getQueryResults( "GetNormalweight", drlPerson1.getId());
        assertTrue(testBodyStyleAux(results,drlPerson1.getId()));

        /*se cambia el peso del usuario para que salga underweight*/
        drlPerson1.setWeight(55);
        kieSession.update(factHandle,drlPerson1);
        kieSession.fireAllRules();
        /*Se comprueba que ahora se ha creado el hecho Underweight*/
        results = kieSession.getQueryResults( "GetUnderweight", drlPerson1.getId());
        assertTrue(testBodyStyleAux(results,drlPerson1.getId()));

        /*Se realizan los mismos pasos para tener sobrepeso y obesidad*/

        drlPerson1.setWeight(85);
        kieSession.update(factHandle,drlPerson1);
        kieSession.fireAllRules();
        results = kieSession.getQueryResults( "GetOverweight", drlPerson1.getId());
        assertTrue(testBodyStyleAux(results,drlPerson1.getId()));

        drlPerson1.setWeight(105);
        kieSession.update(factHandle,drlPerson1);
        kieSession.fireAllRules();
        results = kieSession.getQueryResults( "GetObesity", drlPerson1.getId());
        assertTrue(testBodyStyleAux(results,drlPerson1.getId()));

    }

    /*Función auxiliar para comprobar que el resultado de una query contiene al usuario con el id dado*/
    private boolean testBodyStyleAux(QueryResults results, long id){
        if(results.iterator().hasNext()){
            DrlPerson p = (DrlPerson) results.iterator().next().get("$p");
            return  p.getId().equals(id);
        }
        return false;
    }

    @Test
    public void tetRecommendationExercise() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Se crea un usuario y se inserta.*/
        UserProfile user2 = createUserProfile("user2", new HealthStats(21, 180, 85f, Gender.MALE, LifeStyle.LIGHT_EXERCISE));
        DrlPerson drlPerson1 = DrlConversor.userProfileToDrlPerson(user2);
        FactHandle factHandle = kieSession.insert(drlPerson1);
        SessionPseudoClock clock = kieSession.getSessionClock();
        /*Se insertan una nueva medida*/
        kieSession.insert(new DrlMeasurement(drlPerson1,65,0,0,0,Calendar.getInstance().getTimeInMillis()));
        Calendar c=Calendar.getInstance();
        /*Se establece una hora para el pseudo reloj*/
        c.set(Calendar.HOUR_OF_DAY,21);
        c.set(Calendar.MINUTE,59);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        clock.advanceTime(c.getTimeInMillis(),TimeUnit.MILLISECONDS);

        kieSession.fireAllRules();
        /*Se  avanza un minuto hasta las 22:00 para que salte la regla.*/
        clock.advanceTime(1, TimeUnit.MINUTES);
        /*Se recuperan las notificaciones sin leer.*/
        List<UserNotification> unreadUserNotifications = notificationService.getUnreadUserNotifications(user2.getUsrId(), true);
        /*Se comprueba que se ha añadido la notificación*/
        assertTrue(containsNotification("RECOMMENDATION_OVERWEIGHT",unreadUserNotifications));

        /*Se comprueba para el resto de reglas similares (dependen del imc del usuario), cambiando el peso. Para que vuelvan a ser
        * las 22:00 se avanza el reloj 24h*/
        drlPerson1.setWeight(55);
        kieSession.update(factHandle,drlPerson1);
        kieSession.fireAllRules();
        clock.advanceTime(24,TimeUnit.HOURS);
        unreadUserNotifications = notificationService.getUnreadUserNotifications(user2.getUsrId(), true);
        assertTrue(containsNotification("RECOMMENDATION_UNDERWEIGHT",unreadUserNotifications));

        drlPerson1.setWeight(75);
        kieSession.update(factHandle,drlPerson1);
        kieSession.fireAllRules();
        clock.advanceTime(24,TimeUnit.HOURS);
        unreadUserNotifications = notificationService.getUnreadUserNotifications(user2.getUsrId(), true);
        assertTrue(containsNotification("RECOMMENDATION_NORMALWEIGHT",unreadUserNotifications));

        drlPerson1.setWeight(105);
        kieSession.update(factHandle,drlPerson1);
        kieSession.fireAllRules();
        clock.advanceTime(24,TimeUnit.HOURS);
        unreadUserNotifications = notificationService.getUnreadUserNotifications(user2.getUsrId(), true);
        assertTrue(containsNotification("RECOMMENDATION_OBESITY",unreadUserNotifications));
    }

    @Test
    public void testNoMovement() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Este test comprueba la regla de no movimiento, para ello se crea un usuario y se insertan varias medidas con una separación máxima aproximada de una hora*/
        UserProfile user3 = createUserProfile("user3", new HealthStats(21, 180, 85f, Gender.MALE, LifeStyle.LIGHT_EXERCISE));
        DrlPerson drlPerson1 = DrlConversor.userProfileToDrlPerson(user3);
        FactHandle factHandle = kieSession.insert(drlPerson1);
        SessionPseudoClock clock = kieSession.getSessionClock();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,13);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        c.add(Calendar.MINUTE,-55);
        clock.advanceTime(c.getTimeInMillis(),TimeUnit.MILLISECONDS);

        kieSession.insert(new DrlMeasurement(drlPerson1,65,0,0,0,c.getTimeInMillis()));

        clock.advanceTime(1,TimeUnit.MINUTES);
        kieSession.insert(new DrlMeasurement(drlPerson1,65,0,0,0,c.getTimeInMillis()));
        kieSession.fireAllRules();

        clock.advanceTime(1,TimeUnit.MINUTES);
        kieSession.insert(new DrlMeasurement(drlPerson1,65,0,0,0,c.getTimeInMillis()));
        kieSession.fireAllRules();

        clock.advanceTime(52,TimeUnit.MINUTES);
        kieSession.insert(new DrlMeasurement(drlPerson1,65,0,0,0,c.getTimeInMillis()));
        kieSession.fireAllRules();
        clock.advanceTime(1,TimeUnit.MINUTES);

        List<UserNotification> unreadUserNotifications = notificationService.getUnreadUserNotifications(user3.getUsrId(), true);
        assertTrue(containsNotification("NO_MOVEMENT",unreadUserNotifications));
    }

    @Test
    public void testHighRate() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Este test comprueba las notificaciones de pulsaciones altas, muy altas, y continuadamente altas. Simplemente se empiezan a añadir medidas y se comprueba que se
        * generan las notificaciones.*/
        UserProfile user4 = createUserProfile("user4", new HealthStats(21, 180, 85f, Gender.MALE, LifeStyle.LIGHT_EXERCISE));
        DrlPerson drlPerson1 = DrlConversor.userProfileToDrlPerson(user4);
        FactHandle factHandle = kieSession.insert(drlPerson1);
        SessionPseudoClock clock = kieSession.getSessionClock();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,13);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        clock.advanceTime(c.getTimeInMillis(),TimeUnit.MILLISECONDS);
        kieSession.insert(new DrlMeasurement(drlPerson1,95,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,95,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,200,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,95,0,0,0,c.getTimeInMillis()));
        kieSession.fireAllRules();

        List<UserNotification> unreadUserNotifications = notificationService.getUnreadUserNotifications(user4.getUsrId(), true);
        assertTrue(containsNotification("HIGH_CARDIAC_FREQ",unreadUserNotifications));
        assertTrue(containsNotification("VERY_HIGH_CARDIAC_FREQ",unreadUserNotifications));
        assertTrue(containsNotification("CONTINUOUS_HIGH_CARDIAC_FREQ",unreadUserNotifications));
    }

    @Test
    public void testLowRate() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Este test es similar al de highRate*/
        UserProfile user5 = createUserProfile("user5", new HealthStats(21, 180, 85f, Gender.MALE, LifeStyle.LIGHT_EXERCISE));
        DrlPerson drlPerson1 = DrlConversor.userProfileToDrlPerson(user5);
        FactHandle factHandle = kieSession.insert(drlPerson1);
        SessionPseudoClock clock = kieSession.getSessionClock();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,13);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        clock.advanceTime(c.getTimeInMillis(),TimeUnit.MILLISECONDS);
        kieSession.insert(new DrlMeasurement(drlPerson1,55,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,55,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,49,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,55,0,0,0,c.getTimeInMillis()));
        kieSession.fireAllRules();

        List<UserNotification> unreadUserNotifications = notificationService.getUnreadUserNotifications(user5.getUsrId(), true);
        assertTrue(containsNotification("LOW_CARDIAC_FREQ",unreadUserNotifications));
        assertTrue(containsNotification("VERY_LOW_CARDIAC_FREQ",unreadUserNotifications));
        assertTrue(containsNotification("CONTINUOUS_LOW_CARDIAC_FREQ",unreadUserNotifications));
    }

    @Test
    public void testIrregularRate() throws DuplicateInstanceException, InstanceNotFoundException {
        /*Del mismo modo que los 2 test anteriores pero añadiendo medidas con heartRate muy dispares para que salga la notificación de heart rate irregular.*/
        UserProfile user6 = createUserProfile("user6", new HealthStats(21, 180, 85f, Gender.MALE, LifeStyle.LIGHT_EXERCISE));
        DrlPerson drlPerson1 = DrlConversor.userProfileToDrlPerson(user6);
        FactHandle factHandle = kieSession.insert(drlPerson1);
        SessionPseudoClock clock = kieSession.getSessionClock();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,13);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        clock.advanceTime(c.getTimeInMillis(),TimeUnit.MILLISECONDS);
        kieSession.insert(new DrlMeasurement(drlPerson1,55,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,65,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,105,0,0,0,c.getTimeInMillis()));
        kieSession.insert(new DrlMeasurement(drlPerson1,95,0,0,0,c.getTimeInMillis()));
        kieSession.fireAllRules();
        List<UserNotification> unreadUserNotifications = notificationService.getUnreadUserNotifications(user6.getUsrId(), true);
        assertTrue(containsNotification("IRREGULAR_CARDIAC_FREQ",unreadUserNotifications));
    }
}
