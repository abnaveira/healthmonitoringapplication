package es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice;

import java.util.Objects;

public class DrlMeasurement {
    /*Medida para insetar en el KieSession*/
    private DrlPerson person;/*Usuario para insertar en el KieSession*/
    private int heartRate;/*Pulsaciones*/
    private int steps;/*Pasos*/
    private int distance;/*Distancia*/
    private int calories;/*Calorías*/
    private Long timestamp;/*Momento de creación*/

    public DrlMeasurement(DrlPerson person, int heartRate, int steps, int distance, int calories, Long timestamp) {
        this.person = person;
        this.heartRate = heartRate;
        this.steps = steps;
        this.distance = distance;
        this.calories = calories;
        this.timestamp=timestamp;
    }

    public DrlPerson getPerson() {
        return person;
    }

    public void setPerson(DrlPerson person) {
        this.person = person;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    /*Es necesario sobreescribir el equals para que drools pueda funcionar correctamente.*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrlMeasurement that = (DrlMeasurement) o;
        return heartRate == that.heartRate &&
                steps == that.steps &&
                distance == that.distance &&
                calories == that.calories &&
                Objects.equals(person, that.person) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, heartRate, steps, distance, calories, timestamp);
    }
}
