package es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;

public interface MonitoringService {
    /*Inserta una nueva medida en la session de drools*/
    void insertMeasurement(Measurement measurement);
}
