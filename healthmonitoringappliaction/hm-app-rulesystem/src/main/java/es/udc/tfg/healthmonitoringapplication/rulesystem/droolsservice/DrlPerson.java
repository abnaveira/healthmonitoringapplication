package es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.Gender;
import es.udc.tfg.healthmonitoringapplication.model.daos.healthstats.LifeStyle;

import java.util.Objects;

public class DrlPerson {
    /*Objeto para insertar en el KieSession*/
    private Long id;/*Id del usuario*/
    private int age;/*edad*/
    private int weight;/*peso*/
    private int height;/*altura*/
    private LifeStyle lifeStyle;/*estilo de vida*/
    private Gender gender;/*género*/

    public DrlPerson(Long id, int age, int weight, int height, LifeStyle lifeStyle, Gender gender) {
        this.id = id;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.lifeStyle=lifeStyle;
        this.gender=gender;
    }

    public void update(DrlPerson drlPerson){
        this.age=drlPerson.age;
        this.weight=drlPerson.weight;
        this.height=drlPerson.height;
        this.lifeStyle=drlPerson.lifeStyle;
        this.gender=drlPerson.gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public LifeStyle getLifeStyle() {
        return lifeStyle;
    }

    public void setLifeStyle(LifeStyle lifeStyle) {
        this.lifeStyle = lifeStyle;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


    /*Es necesario sobreescribir el equals para que drools pueda funcionar correctamente.En este caso solo se tiene en cuenta el id de usuario ya que el
    * resto de valores pueden ser modficados.*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrlPerson drlPerson = (DrlPerson) o;
        return Objects.equals(id, drlPerson.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "DrlPerson{" +
                "id=" + id +
                ", age=" + age +
                ", weight=" + weight +
                ", height=" + height +
                ", lifeStyle=" + lifeStyle +
                ", gender=" + gender +
                '}';
    }
}
