package es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice;

import org.drools.core.impl.KnowledgeBaseFactory;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.api.runtime.conf.TimedRuleExecutionOption;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@org.springframework.context.annotation.Configuration
@ComponentScan("es.udc.tfg.healthmonitoringapplication")
/*Clase de configuración para Drools que genera un bean*/
public class Configuration {
    @Bean
    public KieSession kieSession(){
        /*Se crea un builder para poder crear la session*/
        KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        /*Se indica donde está el fichero de reglas*/
        knowledgeBuilder.add(ResourceFactory.newClassPathResource("rules.drl"), ResourceType.DRL);
        /*Se crea un objeto para configurar la base de conocimiento*/
        KieBaseConfiguration kieBaseConfiguration = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
        /*Se establece con forma de procesado en Stream lo que sirve para que se tenga en cuenta el momento de inserción del hecho o evento,
        * la otra opción sería CLOUD lo que no tiene en cuenta el factor temporal.*/
        kieBaseConfiguration.setOption(EventProcessingOption.STREAM);
        KieBase kieBase = knowledgeBuilder.newKieBase();

        /*Configuración para una instancia de la sesión*/
        KieSessionConfiguration sessionConfiguration = KnowledgeBaseFactory.newKnowledgeSessionConfiguration();
        /*Se tiene en cuenta el tiempo de inserción de los hechos*/
        sessionConfiguration.setOption( TimedRuleExecutionOption.YES );
        /*El reloj está a tiempo real, en los tests se usa un psudo-clock para poder avanzar el tiempo de forma programática.*/
        sessionConfiguration.setOption(ClockTypeOption.get("realtime"));
        KieSession kieSession = kieBase.newKieSession(sessionConfiguration,null);
        /*Se devuelve la sesión.*/
        return kieSession;
    }
}
