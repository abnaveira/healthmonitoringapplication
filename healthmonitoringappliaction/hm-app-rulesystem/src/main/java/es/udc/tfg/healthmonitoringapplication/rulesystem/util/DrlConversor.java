package es.udc.tfg.healthmonitoringapplication.rulesystem.util;

import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice.DrlMeasurement;
import es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice.DrlPerson;

public class DrlConversor {
    /*Conversor para los objetos Measurement y UserProfile en los objetos con los que trabajan las reglas.*/


    public static DrlMeasurement measurementToDrlMeasurement(Measurement measurement){
        DrlPerson person=userProfileToDrlPerson(measurement.getUserProfile());
        return new DrlMeasurement(person,measurement.getHeartRate(),measurement.getSteps().intValue(),measurement.getDistance().intValue(),measurement.getCalories().intValue(),measurement.getDate().getTimeInMillis());
    }

    public static DrlPerson userProfileToDrlPerson(UserProfile userProfile){
        return new DrlPerson(userProfile.getUsrId(),userProfile.getHealthStats().getAge(),userProfile.getHealthStats().getWeight().intValue(),
                userProfile.getHealthStats().getHeight(),userProfile.getHealthStats().getLifeStyle(),userProfile.getHealthStats().getGender());
    }
}
