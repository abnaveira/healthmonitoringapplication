package es.udc.tfg.healthmonitoringapplication.rulesystem.droolsservice;

import es.udc.tfg.healthmonitoringapplication.model.daos.measurement.Measurement;
import es.udc.tfg.healthmonitoringapplication.model.daos.userprofile.UserProfile;
import es.udc.tfg.healthmonitoringapplication.model.services.notificationservice.NotificationService;
import es.udc.tfg.healthmonitoringapplication.modelutil.exceptions.InstanceNotFoundException;
import es.udc.tfg.healthmonitoringapplication.rulesystem.util.DrlConversor;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("monitoringService")
public class MonitoringServiceImpl implements MonitoringService {

   @Autowired
   KieSession kieSession;

   @Autowired
    NotificationService notificationService;

    @PostConstruct
    public void postConstruct(){
        /*Se inicializan ciertas variables necesarias en rules.drl localizado en resources*/
        kieSession.setGlobal("notificationService",notificationService);
        kieSession.setGlobal("steps_threshold",50);
    }

    public void insertMeasurement(Measurement measurement){
        /*Se coge el perfil de usuario y se convierte en el objeto que se insertará en la sesión. El propio objeto UserProfile podría servir
        * pero DrlPerson contiene justo la información necesaria y permite una mayor abstracción con respecto al modelo.*/
        DrlPerson drlPerson = DrlConversor.userProfileToDrlPerson(measurement.getUserProfile());
        /*Si el usuario no está insertado dentro de la sesión se inserta*/
        if(!existsDrlPerson(drlPerson)){
            System.out.println("Insert person");
            kieSession.insert(drlPerson);
        }else {
            /*si ya está insertado se actualiza*/
            updateDrlPerson(drlPerson);
        }
        /*Se convierte la medida al objeto que se inserta en la sesión*/
        DrlMeasurement drlMeasurement = DrlConversor.measurementToDrlMeasurement(measurement);
        /*se inserta*/
        kieSession.insert(drlMeasurement);
        /*se disparan todas las reglas. Las propias reglas se encargan de insertar en BD si se genera alguna notificación.*/
        kieSession.fireAllRules();
    }

    /*Comprueba si el objeto ya está insertado en la sesión*/
    private boolean existsDrlPerson(DrlPerson drlPerson){
        /*Las queries para recuperar información de la sesión también están definidas en rules.drl*/
        QueryResults results = kieSession.getQueryResults( "GetPerson",new Object[]{drlPerson.getId()} );
        return results.iterator().hasNext();
    }

    /*Actualiza los campos de DrlPerson en la sesión*/
    private void updateDrlPerson(DrlPerson drlPerson){
        QueryResults results = kieSession.getQueryResults( "GetPerson",new Object[]{drlPerson.getId()} );
        if(results.iterator().hasNext()){
            DrlPerson drlPerson1 = (DrlPerson) results.iterator().next().get("$p");
            FactHandle factHandle = kieSession.getFactHandle(drlPerson1);
            drlPerson1.update(drlPerson);
            System.out.println(drlPerson1);
            kieSession.update(factHandle,drlPerson1);
        }

    }
}
